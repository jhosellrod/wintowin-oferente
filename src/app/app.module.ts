import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { CartTabDefaultPagePage } from '../pages/cart-tab-default-page/cart-tab-default-page';
import { ContactoPage } from '../pages/contacto/contacto';
import { TabsControllerPage } from '../pages/tabs-controller/tabs-controller';
import { RegistroConductor1Page } from '../pages/registro-conductor1/registro-conductor1';
import { RegistroConductor2Page } from '../pages/registro-conductor2/registro-conductor2';
import { BienvenidaPage } from '../pages/bienvenida/bienvenida';
import { RequisitosPage } from '../pages/requisitos/requisitos';
import { DatosDelVehCuloPage } from '../pages/datos-del-veh-culo/datos-del-veh-culo';
import { DocumentaciNVehCuloPage } from '../pages/documentaci-nveh-culo/documentaci-nveh-culo';
import { MiPerfilPage } from '../pages/mi-perfil/mi-perfil';
import { ConfirmaciNCambiosPage } from '../pages/confirmaci-ncambios/confirmaci-ncambios';
import { ConfirmaciNDocumentaciNConductorPage } from '../pages/confirmaci-ndocumentaci-nconductor/confirmaci-ndocumentaci-nconductor';
import { ConfirmaciNDocumentaciNVehCuloPage } from '../pages/confirmaci-ndocumentaci-nveh-culo/confirmaci-ndocumentaci-nveh-culo';
import { ConfirmaciNRegistroUsuarioConductorPage } from '../pages/confirmaci-nregistro-usuario-conductor/confirmaci-nregistro-usuario-conductor';
import { ConfirmaciNCambioDeRutaPage } from '../pages/confirmaci-ncambio-de-ruta/confirmaci-ncambio-de-ruta';
import { ConfirmaciNCancelarViajePage } from '../pages/confirmaci-ncancelar-viaje/confirmaci-ncancelar-viaje';
import { ConfirmaciNMensajePage } from '../pages/confirmaci-nmensaje/confirmaci-nmensaje';
import { ConfirmaciNRespuestaEnviadaPage } from '../pages/confirmaci-nrespuesta-enviada/confirmaci-nrespuesta-enviada';
import { ConfirmaciNContraseAPage } from '../pages/confirmaci-ncontrase-a/confirmaci-ncontrase-a';
import { ConfirmaciNCreaciNDeViajePage } from '../pages/confirmaci-ncreaci-nde-viaje/confirmaci-ncreaci-nde-viaje';
import { ConfirmaciNReporteDeViajePage } from '../pages/confirmaci-nreporte-de-viaje/confirmaci-nreporte-de-viaje';
import { ConfirmaciNAceptaciNUsuarioPage } from '../pages/confirmaci-naceptaci-nusuario/confirmaci-naceptaci-nusuario';
import { MiGrupoPage } from '../pages/mi-grupo/mi-grupo';
import { ViajesRealizadosPage } from '../pages/viajes-realizados/viajes-realizados';
import { ResultadosViajesRealizadosPage } from '../pages/resultados-viajes-realizados/resultados-viajes-realizados';
import { MiActualViajePage } from '../pages/mi-actual-viaje/mi-actual-viaje';
import { WalletPage } from '../pages/wallet/wallet';
import { CalificacionesPage } from '../pages/calificaciones/calificaciones';
import { PagoDeMembresiasPage } from '../pages/pago-de-membresias/pago-de-membresias';
import { VerUsuarioPage } from '../pages/ver-usuario/ver-usuario';
import { MensajeRecibidoPage } from '../pages/mensaje-recibido/mensaje-recibido';
import { ResponderMensajePage } from '../pages/responder-mensaje/responder-mensaje';
import { Contacto2Page } from '../pages/contacto2/contacto2';
import { IniciaSesionPage } from '../pages/inicia-sesion/inicia-sesion';
import { RecordarContraseAPage } from '../pages/recordar-contrase-a/recordar-contrase-a';
import { CrearUnViajePage } from '../pages/crear-un-viaje/crear-un-viaje';
import { EstadoDelViajePage } from '../pages/estado-del-viaje/estado-del-viaje';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Geolocation } from '@ionic-native/geolocation';
import { Network } from '@ionic-native/network';
import { CorekProvider } from '../providers/corek/corek';
import { IonicStorageModule } from '@ionic/storage';
import { Camera } from '@ionic-native/camera';
import { FCM } from '@ionic-native/fcm';
import { ListMessagesPage } from '../pages/list-messages/list-messages';
import { BackgroundGeolocation} from '@ionic-native/background-geolocation';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { PayPage } from '../pages/pay/pay';
import { File } from '@ionic-native/file';
import { FileTransfer } from '@ionic-native/file-transfer';
import { TerminosPage } from "../pages/terminos/terminos";

@NgModule({
  declarations: [
    MyApp,
    CartTabDefaultPagePage,
    ContactoPage,
    TabsControllerPage,
    RegistroConductor1Page,
    RegistroConductor2Page,
    BienvenidaPage,
    RequisitosPage,
    DatosDelVehCuloPage,
    DocumentaciNVehCuloPage,
    MiPerfilPage,
    ConfirmaciNCambiosPage,
    ConfirmaciNDocumentaciNConductorPage,
    ConfirmaciNDocumentaciNVehCuloPage,
    ConfirmaciNRegistroUsuarioConductorPage,
    ConfirmaciNCambioDeRutaPage,
    ConfirmaciNCancelarViajePage,
    ConfirmaciNMensajePage,
    ConfirmaciNRespuestaEnviadaPage,
    ConfirmaciNContraseAPage,
    ConfirmaciNCreaciNDeViajePage,
    ConfirmaciNReporteDeViajePage,
    ConfirmaciNAceptaciNUsuarioPage,
    MiGrupoPage,
    ViajesRealizadosPage,
    ResultadosViajesRealizadosPage,
    MiActualViajePage,
    WalletPage,
    CalificacionesPage,
    PagoDeMembresiasPage,
    VerUsuarioPage,
    MensajeRecibidoPage,
    ResponderMensajePage,
    Contacto2Page,
    IniciaSesionPage,
    RecordarContraseAPage,
    CrearUnViajePage,
    EstadoDelViajePage,
    ListMessagesPage,
    PayPage,
    TerminosPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    IonicStorageModule.forRoot({
      name: '__winToWinHero',
         driverOrder: [ 'sqlite', 'websql']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    CartTabDefaultPagePage,
    ContactoPage,
    TabsControllerPage,
    RegistroConductor1Page,
    RegistroConductor2Page,
    BienvenidaPage,
    RequisitosPage,
    DatosDelVehCuloPage,
    DocumentaciNVehCuloPage,
    MiPerfilPage,
    ConfirmaciNCambiosPage,
    ConfirmaciNDocumentaciNConductorPage,
    ConfirmaciNDocumentaciNVehCuloPage,
    ConfirmaciNRegistroUsuarioConductorPage,
    ConfirmaciNCambioDeRutaPage,
    ConfirmaciNCancelarViajePage,
    ConfirmaciNMensajePage,
    ConfirmaciNRespuestaEnviadaPage,
    ConfirmaciNContraseAPage,
    ConfirmaciNCreaciNDeViajePage,
    ConfirmaciNReporteDeViajePage,
    ConfirmaciNAceptaciNUsuarioPage,
    MiGrupoPage,
    ViajesRealizadosPage,
    ResultadosViajesRealizadosPage,
    MiActualViajePage,
    WalletPage,
    CalificacionesPage,
    PagoDeMembresiasPage,
    VerUsuarioPage,
    MensajeRecibidoPage,
    ResponderMensajePage,
    Contacto2Page,
    IniciaSesionPage,
    RecordarContraseAPage,
    CrearUnViajePage,
    EstadoDelViajePage,
    ListMessagesPage,
    PayPage,
    TerminosPage
  ],
  providers: [
    StatusBar,
    InAppBrowser,
    SplashScreen,
    Geolocation,
    Network,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    CorekProvider,
    Camera,
    LocationAccuracy,
    FCM,
    BackgroundGeolocation,
    File,
    FileTransfer
  ]
})
export class AppModule { }