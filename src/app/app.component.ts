import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, AlertController, LoadingController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { MiPerfilPage } from '../pages/mi-perfil/mi-perfil';
import { IniciaSesionPage } from '../pages/inicia-sesion/inicia-sesion';
import { CrearUnViajePage } from '../pages/crear-un-viaje/crear-un-viaje';
import { MiActualViajePage } from '../pages/mi-actual-viaje/mi-actual-viaje';
import { Storage } from '@ionic/storage';
import { ViajesRealizadosPage } from '../pages/viajes-realizados/viajes-realizados';
import { MiGrupoPage } from '../pages/mi-grupo/mi-grupo';
import { WalletPage } from '../pages/wallet/wallet';
import { Contacto2Page } from '../pages/contacto2/contacto2';
import { FCM } from '@ionic-native/fcm'
import { CalificacionesPage } from '../pages/calificaciones/calificaciones';
import { PayPage } from '../pages/pay/pay';
import { DocumentaciNVehCuloPage } from '../pages/documentaci-nveh-culo/documentaci-nveh-culo';
import { DatosDelVehCuloPage } from '../pages/datos-del-veh-culo/datos-del-veh-culo';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  public id: any;
  @ViewChild(Nav) navCtrl: Nav;
  rootPage:any ;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public storage : Storage, public alertCtrl: AlertController, public loadingCtrl: LoadingController, private fcm: FCM) {

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

    this.storage.get('remember').then((val) => {
      console.log('Your token is', val);
      if(val == false || val == null ){
        this.rootPage = IniciaSesionPage;
      }else{
        this.storage.get('session').then((r) => {
          console.log("Your session is ", r)
          if(r == null || r == ""){
            splashScreen.hide();
            this.rootPage = IniciaSesionPage;
          }else{
            splashScreen.hide();
            this.rootPage = CrearUnViajePage;
          }
        });
      }
    });
  }

  goToMiPerfil(){
    this.navCtrl.setRoot(MiPerfilPage);
    this.navCtrl.popToRoot();
  }

  goToMiActualViaje(){
    this.navCtrl.setRoot(MiActualViajePage);
    this.navCtrl.popToRoot();
  }

  goToWallet(){
    this.navCtrl.setRoot(WalletPage);
    this.navCtrl.popToRoot();
  }

  goToContacto2(){
    this.navCtrl.setRoot(Contacto2Page);
    this.navCtrl.popToRoot();
  }

  goToViajesRealizados(){
    this.navCtrl.setRoot(ViajesRealizadosPage);
    this.navCtrl.popToRoot();
  }

  goToMiGrupo(){
    this.navCtrl.setRoot(MiGrupoPage);
    this.navCtrl.popToRoot();
  }

  goToCali(){
    this.navCtrl.setRoot(CalificacionesPage);
    this.navCtrl.popToRoot();
  }

  logout(){
    let alert = this.alertCtrl.create({
      title: 'Cerrar sesion',
      message: '¿Seguro que deseas cerrar la sesion?',
      buttons: [
        {
          text: 'Confirmar',
          handler: () => {
            this.storage.get('user_id').then((data)=>{
              this.fcm.unsubscribeFromTopic('notification'+data);
              this.storage.clear();
              this.navCtrl.setRoot(IniciaSesionPage);
              this.navCtrl.popToRoot();
            });
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }

  cali(){
    this.navCtrl.setRoot(CalificacionesPage);
    this.navCtrl.popToRoot();
  }
  
  goToPagoDeMembresias(){
    this.navCtrl.setRoot(PayPage);
    this.navCtrl.popToRoot();
  }
}
