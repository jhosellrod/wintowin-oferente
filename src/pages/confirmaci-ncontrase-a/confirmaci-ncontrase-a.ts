import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { IniciaSesionPage } from '../inicia-sesion/inicia-sesion';


@Component({
  selector: 'page-confirmaci-ncontrase-a',
  templateUrl: 'confirmaci-ncontrase-a.html'
})
export class ConfirmaciNContraseAPage {

  constructor(public navCtrl: NavController) {}
  goToIniciaSesion(params){
    if (!params) params = {};
    this.navCtrl.push(IniciaSesionPage);
  }
}
