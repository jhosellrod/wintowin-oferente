import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { CrearUnViajePage } from '../crear-un-viaje/crear-un-viaje';
import { ConfirmaciNCreaciNDeViajePage } from '../confirmaci-ncreaci-nde-viaje/confirmaci-ncreaci-nde-viaje';
import { EstadoDelViajePage } from '../estado-del-viaje/estado-del-viaje';
import { VerUsuarioPage } from '../ver-usuario/ver-usuario';
import { ConfirmaciNReporteDeViajePage } from '../confirmaci-nreporte-de-viaje/confirmaci-nreporte-de-viaje';

@Component({
  selector: 'page-confirmaci-ncancelar-viaje',
  templateUrl: 'confirmaci-ncancelar-viaje.html'
})
export class ConfirmaciNCancelarViajePage {

  constructor(public navCtrl: NavController) {
  }
  goToCrearUnViaje(params){
    if (!params) params = {};
    this.navCtrl.push(CrearUnViajePage);
  }goToConfirmaciNCreaciNDeViaje(params){
    if (!params) params = {};
    this.navCtrl.push(ConfirmaciNCreaciNDeViajePage);
  }goToEstadoDelViaje(params){
    if (!params) params = {};
    this.navCtrl.push(EstadoDelViajePage);
  }goToVerUsuario(params){
    if (!params) params = {};
    this.navCtrl.push(VerUsuarioPage);
  }goToConfirmaciNReporteDeViaje(params){
    if (!params) params = {};
    this.navCtrl.push(ConfirmaciNReporteDeViajePage);
  }
}
