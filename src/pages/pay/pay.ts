import { Component } from '@angular/core';
import { IonicPage, LoadingController ,NavController, NavParams, ToastController } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Storage } from '@ionic/storage'
import { PagoDeMembresiasPage } from '../pago-de-membresias/pago-de-membresias';
import { CorekProvider } from '../../providers/corek/corek';
import moment from 'moment';

/**
 * Generated class for the PayPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pay',
  templateUrl: 'pay.html',
})
export class PayPage {
  public id : any;
  public dataPay: any;
  public idPost: any;
  public licenses_end: any;
  licenses_days;

  constructor(public loadingCtrl: LoadingController, private corek: CorekProvider, public toast: ToastController, public navCtrl: NavController, public navParams: NavParams, private storage: Storage, private iab: InAppBrowser) {
    this.licenses_end = this.navParams.get('licenses_end');
  }

  ionViewDidEnter(){
    this.storage.get('ID').then((id)=>{
      this.id = id;
      let nf = Date.now().toString()+Math.random();
      this.corek.socket.emit('get_users',{"condition":{'ID':this.id},'event':nf});
      this.corek.socket.on(nf, (data,key)=>{
        console.log(data)
        let a = moment();
        let b = moment(data[0].user_registered_end);
        this.licenses_days = b.diff(a, 'days');
        console.log(this.licenses_days);
      });
    });
  }

  
  pay(){
    let loader = this.loadingCtrl.create({
      content: "Espere",
      duration: 15000
    });
    loader.present();
    console.log(this.dataPay);
    if(this.dataPay == undefined || this.dataPay == '' || this.dataPay == null){
      let toast = this.toast.create({
        message: 'Selecciona un monto a pagar',
        duration: 3000
      });
      toast.present();
      loader.dismiss();
    }else{
      let time = Date.now().toString();
      const tomorrow = new Date();
    
      // tomorrow.setDate();
      let xd =  tomorrow.getDate();
      let xm =  tomorrow.getUTCMonth();
      let xy =  tomorrow.getFullYear();
      xm = xm + 1;
      let date = xy + "-" + xm +"-"+ xd; 
      this.corek.socket.emit('query',{'event':time + 'query', 'querystring':"SELECT * FROM `wp_users` WHERE `ID` = "+this.id});
      this.corek.socket.on(time + 'query', (result1, key)=>{
        this.corek.socket.emit('insert_post',{'condition':{'post_type':'transaction','post_author':this.id,'post_status':'publish','post_content':this.dataPay,'post_date': date},'event':time +'insertTran'});    
        this.corek.socket.on(time +'insertTran', (result, key)=>{
          this.idPost = result.insertId;
          loader.dismiss();
          this.iab.create("http://159.203.82.152/api/api.php?id="+this.id+"&amount="+this.dataPay+"&email="+result1[0].user_email+"&ref="+this.idPost+"&codeGroup="+result1[0].user_activation_key);
        });
      });
    }
  }

  // pay(){ verifica si esta publish
  //   if(this.dataPay == undefined || this.dataPay == '' || this.dataPay == null){
  //     let toast = this.toast.create({
  //       message: 'Selecciona un monto a pagar',
  //       duration: 3000
  //     });
  //     toast.present();
  //   }else{
  //     let time = Date.now().toString();
  //     this.corek.socket.emit('query',{'event':time + 'query', 'querystring':"SELECT * FROM `wp_posts` WHERE `post_author` = "+this.id+" AND `post_status` LIKE 'publish' AND `post_type` LIKE 'transaction'"});
  //     this.corek.socket.on(time + 'query', (result, key)=>{
  //       if(result.length > 0){
  //         let toast = this.toast.create({
  //           message: 'Tiene una transaccion pendiente.',
  //           duration: 3000
  //         });
  //         toast.present();
  //       }else{
  //         this.corek.socket.emit('insert_post',{'condition':{'post_type':'transaction','post_author':this.id,'post_status':'publish'},'event':time +'insertTran'});    
  //         this.iab.create("http://159.203.82.152/api/api.php?id="+this.id+"&amount="+this.dataPay);
  //         // this.iab.create("http://159.203.82.152/api.php?id="+this.id+"","_blank");
  //       }
  //     });
  //   }
  // }


  history(){
    this.navCtrl.setRoot(PagoDeMembresiasPage); // this is history
    this.navCtrl.popToRoot();
  }
}