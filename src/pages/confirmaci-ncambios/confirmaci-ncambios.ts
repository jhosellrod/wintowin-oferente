import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { CrearUnViajePage } from '../crear-un-viaje/crear-un-viaje';
import { ConfirmaciNCreaciNDeViajePage } from '../confirmaci-ncreaci-nde-viaje/confirmaci-ncreaci-nde-viaje';
import { EstadoDelViajePage } from '../estado-del-viaje/estado-del-viaje';
import { VerUsuarioPage } from '../ver-usuario/ver-usuario';
import { ConfirmaciNReporteDeViajePage } from '../confirmaci-nreporte-de-viaje/confirmaci-nreporte-de-viaje';

@Component({
  selector: 'page-confirmaci-ncambios',
  templateUrl: 'confirmaci-ncambios.html'
})
export class ConfirmaciNCambiosPage {

  constructor(public navCtrl: NavController) {
  }
  goToCrearUnViaje(params){
    if (!params) params = {};
    this.navCtrl.setRoot(CrearUnViajePage);
    this.navCtrl.popToRoot();
    // this.navCtrl.push(CrearUnViajePage);

  }goToConfirmaciNCreaciNDeViaje(params){
    if (!params) params = {};
    this.navCtrl.push(ConfirmaciNCreaciNDeViajePage);
  }goToEstadoDelViaje(params){
    if (!params) params = {};
    this.navCtrl.push(EstadoDelViajePage);
  }goToVerUsuario(params){
    if (!params) params = {};
    this.navCtrl.push(VerUsuarioPage);
  }goToConfirmaciNReporteDeViaje(params){
    if (!params) params = {};
    this.navCtrl.push(ConfirmaciNReporteDeViajePage);
  }
}
