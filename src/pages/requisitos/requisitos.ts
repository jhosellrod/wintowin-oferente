import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { RegistroConductor1Page } from '../registro-conductor1/registro-conductor1';


@Component({
  selector: 'page-requisitos',
  templateUrl: 'requisitos.html'
})
export class RequisitosPage {

  constructor(public navCtrl: NavController) {
  }
  goToRegistroConductor1(params){
    if (!params) params = {};
    this.navCtrl.push(RegistroConductor1Page);
  }
}
