import { Component} from '@angular/core';
import { NavController, LoadingController, AlertController, ActionSheetController } from 'ionic-angular';
import { CorekProvider } from '../../providers/corek/corek';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IniciaSesionPage } from '../inicia-sesion/inicia-sesion';
import { RegistroConductor2Page } from '../registro-conductor2/registro-conductor2';
import { RequisitosPage } from '../requisitos/requisitos';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { TerminosPage } from "../terminos/terminos";
import { ModalController } from 'ionic-angular';


@Component({
  selector: 'page-registro-conductor1',
  templateUrl: 'registro-conductor1.html'
})
export class RegistroConductor1Page {

  image = '';
  form: FormGroup;
  mismatch = false;
  show: boolean = false
  passwordType: string = 'password';
  passwordIcon: string = 'eye';
  repasswordType: string = 'password';
  repasswordIcon: string = 'eye';
  checkbox;
  constructor(public formBuilder: FormBuilder, public navCtrl: NavController, public loadingCtrl: LoadingController, private corek: CorekProvider, public alertCtrl: AlertController, private camera: Camera , public actionSheetCtrl: ActionSheetController, private transfer: FileTransfer, private file: File, public modalCtrl: ModalController) {
    this.form = formBuilder.group({
      phone: ['', [Validators.required,Validators.maxLength(10),Validators.minLength(10)]],
      email: ['', [Validators.required, Validators.email]],
      name: ['', [Validators.required,Validators.pattern(/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*$/)]],
      lastname: ['', [Validators.required,Validators.pattern(/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*$/)]],
      password: ['', [Validators.required,Validators.minLength(8)]],
      repassword: ['', [Validators.required,Validators.minLength(8)]],
      terms: [false, [Validators.required]]
    },{validator: this.passwordsMatch});
  }
  
  passwordsMatch(cg: FormGroup){
    let pwd1 = cg.get('password').value;
    let pwd2 = cg.get('repassword').value;
    if ((pwd1 && pwd2) && pwd1 !== pwd2) {
      cg.controls['repassword'].setErrors({"pw_mismatch": true});
      return {"pw_mismatch": true};
    } else{
      return null;
    }
  }

  register(){
    let loading = this.loadingCtrl.create({
      content: "Espere por favor",
      duration: 15000
    });
    loading.present();
    if( !this.validate_email(this.form.value.email) ){
      this.error();
      loading.dismiss();
    }else{
      let n = Date.now().toString+'cone'+Math.random();
      this.corek.ConnectCorekconfig(n);
      this.corek.socket.on(n, (dat, key)=>{
        console.log(dat);
        let nf1 = Date.now().toString() +'get_user'+Math.random();
        this.corek.socket.emit('get_users', {'condition':{'user_email':this.form.value.email}, 'event':nf1}); 
        this.corek.socket.on(nf1, (result , key)=>{
          if(result.length > 0){
            loading.dismiss();
            let alert = this.alertCtrl.create({
              title: 'Hola '+this.form.value.name,
              subTitle: 'El correo: '+this.form.value.email+' ya se encuentra registrado',
              buttons: ['OK']
            });
            alert.present();
          }else{
            loading.dismiss();
            let user_data = {'email': this.form.value.email, 'pass':this.form.value.password, 'name':this.form.value.name, 'lastname': this.form.value.lastname, 'phone': this.form.value.phone, 'picture': this.image}
            this.navCtrl.setRoot(RegistroConductor2Page, {user:user_data});
            this.navCtrl.popToRoot();
          }
        });
      });
    }
  }

  error(){
    let alert = this.alertCtrl.create({
      title: 'Error',
      message: 'Ingrese un correo valido',
      buttons: [        
        {
          text: 'OK',
          handler: () => {              
          }
        }
      ]
    });
    alert.present();
  }

  validate_email( email ) {
    var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email) ? true : false;
  }

  upload_picture(){
    let loading = this.loadingCtrl.create({
      content: "Espere",
      duration: 5000
    });
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Imagen de perfil',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Destructive clicked');
          }
        },{
          text: 'Camara',
          handler: () => {
            console.log('Cam clicked');
            const options: CameraOptions = {
              quality: 100,
              sourceType: this.camera.PictureSourceType.CAMERA,
              destinationType: this.camera.DestinationType.NATIVE_URI,
              encodingType: this.camera.EncodingType.JPEG,
              mediaType: this.camera.MediaType.PICTURE ,
              correctOrientation : false,
              allowEdit: true,
              targetWidth: 100,
              targetHeight: 100,
            }
            this.camera.getPicture(options).then((imageData) => {
              let d = new Date();
              let st = d.getDate() +"-"+ d.getMonth() +"-"+ d.getFullYear() +"-"+ d.getHours() +"-"+ d.getMinutes() +"-"+ d.getSeconds() +"-"+ d.getMilliseconds();
              const namefile =  st + "-"+ this.form.value.email + "-"+ 'cam.jpg' ; 
              let options1: FileUploadOptions = {
                fileKey: 'file',
                fileName: namefile,
                headers: {}
              }
              const fileTransfer: FileTransferObject = this.transfer.create();
              loading.present();
              fileTransfer.upload(imageData, 'http://159.203.82.152/custom-upload.php', options1).then((data) => {
                  this.image = namefile;
                }, (err) => {
                  alert('Error. Intente de nuevo')
                });
              }, (err) => {
            });
          }
        },{
          text: 'Galeria',
          handler: () => {
            console.log('Gal clicked');
            const options: CameraOptions = {
              quality: 100,
              sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
              destinationType: this.camera.DestinationType.FILE_URI,
              encodingType: this.camera.EncodingType.JPEG,
              mediaType: this.camera.MediaType.PICTURE,
              correctOrientation : false,
              allowEdit: true,
              targetWidth: 100,
              targetHeight: 100,
            }
            this.camera.getPicture(options).then((imageData) => {
              let d = new Date();
              let st = d.getDate() +"-"+ d.getMonth() +"-"+ d.getFullYear() +"-"+ d.getHours() +"-"+ d.getMinutes() +"-"+ d.getSeconds() +"-"+ d.getMilliseconds();
              const namefile =  st + "-"+ this.form.value.email + "-"+ 'gal.jpg' ; 
              let options1: FileUploadOptions = {
                fileKey: 'file',
                fileName: namefile,
                headers: {}
              }
              const fileTransfer: FileTransferObject = this.transfer.create();
              loading.present();
              fileTransfer.upload(imageData, 'http://159.203.82.152/custom-upload.php', options1).then((data) => {
                  this.image = namefile;
                }, (err) => {
                  alert('Error. Intente de nuevo');
                });
          });
        }
        }
      ]
    });
    actionSheet.present();
  }

  showHide(){
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  showHideRe(){
    this.repasswordType = this.repasswordType === 'text' ? 'password' : 'text';
    this.repasswordIcon = this.repasswordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  terms_condition(){
    const modal = this.modalCtrl.create(TerminosPage);
    modal.present();
  }

  goToIniciaSesion(params){
    if (!params) params = {};
    this.navCtrl.push(IniciaSesionPage);
  }goToRegistroConductor2(params){
    if (!params) params = {};
    this.navCtrl.push(RegistroConductor2Page);
  }goToRequisitos(params){
    if (!params) params = {};
    this.navCtrl.push(RequisitosPage);
  }
}
