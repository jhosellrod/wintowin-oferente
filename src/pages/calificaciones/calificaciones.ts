import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController} from 'ionic-angular';
import { CorekProvider } from '../../providers/corek/corek';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-calificaciones',
  templateUrl: 'calificaciones.html'
})
export class CalificacionesPage {
  public percentage;
  public name: any;
  public lastName: any;
  public photo: any;
  public items = [];
  public id: any;
  bandera = true;
  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, private corek: CorekProvider, private storage: Storage) {}

  ionViewDidEnter(){
    this.storage.get('ID').then(data=>{
      this.id = data;
      const time = Date.now().toString()+Math.random();
      let loader = this.loadingCtrl.create({
        content: "Espere",
        duration: 15000
      });
      loader.present();
      this.corek.socket.emit('query',{'event':time + 'queryb', 'querystring':"SELECT * FROM `wp_posts` WHERE `post_type` LIKE 'calificationHero' AND `to_ping` = "+this.id});
      this.corek.socket.on(time + 'queryb', (data, key) => {
        console.log(data);
        if(data.length > 0){
          this.bandera = false;
          for(let i of data){
            let average = i.post_title/i.post_excerpt;
            this.percentage = average*100/5;
            let start;
            if(this.percentage <= 15) start = 0; 
            else if(this.percentage > 15 && this.percentage < 50) start = 1;
            else if(this.percentage == 50) start = 2;
            else if(this.percentage > 50 && this.percentage <= 90) start = 3;
            else if(this.percentage > 90 && this.percentage <= 100) start = 4;
            const time1 = Date.now().toString + i.post_author+Math.random();
            let r = Math.random();
            this.corek.socket.emit('query',{'event':time1 + 'query1b'+r, 'querystring':"SELECT * FROM `wp_users` WHERE `ID` = "+i.post_author});
            this.corek.socket.on(time1 + 'query1b'+r, (data1, key) => {
              console.log(data1)
              this.name = data1[0].user_url;
              this.lastName = data1[0].user_nicename;
              this.photo = data1[0].display_name;
              loader.dismiss()
              this.items.push({'average':average.toFixed(2), 'start':start,'comment':i.post_content, 'name':this.name,'lastName':this.lastName,'photo':this.corek.ipServe()+this.photo});
            });
          }
        }else{
          this.bandera = true;
          loader.dismiss();
        }
      });
    })
  }

  /* if(data.length > 0){
    for(let i of data){
      let average = i.post_title/i.post_excerpt;
      this.percentage = average*100/5;
      let start;
      console.log(this.percentage);
      if(this.percentage <= 15) start = 0; 
      else if(this.percentage > 15 && this.percentage < 50) start = 1;
      else if(this.percentage == 50) start = 2;
      else if(this.percentage > 50 && this.percentage <= 90) start = 3;
      else if(this.percentage > 90 && this.percentage <= 100) start = 4;
      console.log(this.percentage);
      const time1 = Date.now().toString + i.post_author;
      this.corek.socket.emit('query',{'event':time1 + 'query1', 'querystring':"SELECT * FROM `wp_users` WHERE `ID` = "+i.post_author});
      this.corek.socket.on(time1 + 'query1', (data1, key) => {
        this.name = data1[0].user_url;
        this.lastName = data1[0].user_nicename;
        this.photo = data1[0].display_name;
        this.items.push({'average':average.toFixed(2), 'start':start,'comment':i.post_content, 'name':this.name,'lastName':this.lastName,'photo':this.photo});
        console.log(this.items);
      });
    }
  } */
}
