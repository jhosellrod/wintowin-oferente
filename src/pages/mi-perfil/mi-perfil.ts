import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { CorekProvider } from '../../providers/corek/corek';
import { Storage } from '@ionic/storage';
import { LoadingController } from 'ionic-angular'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController } from 'ionic-angular';
import moment from 'moment'
import { ConfirmaciNCambiosPage } from '../confirmaci-ncambios/confirmaci-ncambios';

@Component({
  selector: 'page-mi-perfil',
  templateUrl: 'mi-perfil.html'
})
export class MiPerfilPage {
  public id: any;
  public email: any;
  public pwd: any;
  public code: any;
  public md5: '';
  public showName: any;
  public showLastName: any;
  public phone: any;
  public type: any;
  public emailCorp1: any;
  public emailCorp2: any;
  public numLic: any;
  public newNumLic: any;
  public getJ: any;
  public getJ1: any;
  public dataLicen;
  public communityValue: any;
  licenses;
  numberDoc;
  communitySelect;
  communitys = [];
  form: FormGroup;
  aux;
  auxName = [];
  flag;
  codeCar;
  constructor(public toastCtrl: ToastController,private alertCtrl: AlertController, public formBuilder: FormBuilder,public loadingCtrl: LoadingController, public storage : Storage, private corek: CorekProvider, public navCtrl: NavController) {
    this.form = formBuilder.group({
      name: [[Validators.required,Validators.pattern(/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*$/)]],
      lastName: [[Validators.required,Validators.pattern(/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*$/)]],
      email1: [[Validators.required, Validators.email]],
      email2: [[Validators.required, Validators.email]],
      phone: [[Validators.required,Validators.maxLength(10),Validators.minLength(10)]],
        community: [],
    });
    this.storage.get('ID').then(data=>{
      this.id = data;
      this.getDataUser();
    });
    console.log(this.form.value.email2);
  }
  
  getDataUser(){// Occasionally it brings wrong dates. Delete console.log
    let loader = this.loadingCtrl.create({
      content: "Espere",
      duration: 15000
    });
    loader.present();
    const time = Date.now().toString();
    this.corek.socket.emit('query',{'event':time + "consult", 'querystring':"SELECT * FROM `wp_usermeta` WHERE `meta_key` LIKE 'community' AND `user_id` = "+this.id});
    this.corek.socket.on(time + "consult",(data,key)=> {
      let nf = Date.now().toString()+Math.random();
      console.log(data); 
      if (data.length == 0) { 
      this.corek.socket.emit('get_users',{"condition":{'ID':this.id},'event':nf +'get_user'});
      this.corek.socket.on(nf+'get_user', (user,key)=>{
        console.log('mosntrando');
        console.log(user);
        let a = moment();
        let b = moment(user[0].user_registered_end)
        this.licenses = b.diff(a, 'days')
        this.showName=user[0].user_url;
        this.showLastName=user[0].user_nicename;
        this.code=user[0].user_activation_key;
        this.codeCar=user[0].code_car;
        this.email=user[0].user_email;
        this.phone=user[0].user_login;
        this.getMeta();
        loader.dismiss(); 
      });
      this.corek.socket.emit('query_post' , {condition:{'post_type':'Communities'}, 'event':nf+"c0mmu"});
      this.corek.socket.on(nf+"c0mmu", (community, key) => {
        console.log(community);
        // this.communitySelect = [];
        let selectedthis=false;
        for (let i in community){
          selectedthis=false;
          for(let nr of this.communitySelect){
            if(nr == community[i].post_content){
              selectedthis=true;
              this.aux =1;
            }
          }
          this.communitys.push({name:community[i].post_content,selected:selectedthis});
        }             
      });
      } else {
        console.log("sd");
        this.communitySelect = JSON.parse(data[0].meta_value);
        // let ID = this.getId();
        if(this.id != undefined){
          // consulta de comunidades
          this.corek.socket.emit('query_post' , {condition:{'post_type':'Communities'}, 'event':nf+"commu1"});
          this.corek.socket.on(nf+"commu1", (community, key) => {
            console.log(community);
            // this.communitySelect = [];
            let selectedthis=false;
            for (let i in community){
              selectedthis=false;
              for(let nr of this.communitySelect){
                if(nr == community[i].post_content){
                  selectedthis=true;
                  this.aux =1;
                }
              }
              this.communitys.push({name:community[i].post_content,selected:selectedthis});
            }
            console.log( this.communitys);
            this.corek.socket.emit('get_users',{"condition":{'ID':this.id},'event':nf +'get_user'});
            this.corek.socket.on(nf+'get_user', (data,key)=>{
              let a = moment();
              let b = moment(data[0].user_registered_end)
              this.licenses = b.diff(a, 'days')
              this.showName=data[0].user_url;
              this.showLastName=data[0].user_nicename;
              this.codeCar = data[0].code_car;
              this.code=data[0].user_activation_key;
              this.email=data[0].user_email;
              this.phone=data[0].user_login;
              this.getMeta();
              loader.dismiss(); 
            });
          });
        }else{
          this.error();
          loader.dismiss(); 
        } 
      }
    });     
  }

  getCommunity(evt){
    console.log(evt)
    this.auxName = evt;
    if(this.form.value.community.length > 3){
      let alert = this.alertCtrl.create({
        title: 'Muchas comunidades',
        subTitle: 'Solo puedes escoger tres (03) comunidades',
        buttons: ['OK']
      });
      this.form.value.community.length = 0;
      alert.present();
    }else if(this.form.value.community.length > 0){
      for(let e of evt){
        console.log(e)
        for(let c of this.communitys){
          if(c.name == e){
              c.selected = true;  
              this.aux = 1;
              console.log(c);
          }
        }
      }  
    console.log(this.communitys);
    }else if(this.form.value.community.length == 0){
      for(let c of this.communitys){
        c.selected = false;  
      }
    }
    
  }

  getMeta(){
    let loader = this.loadingCtrl.create({
      content: "Espere"
    });
    loader.present();
    let getJson;
    const time = Date.now().toString()+Math.random();
    this.corek.socket.emit('get_user_meta',{"condition":{"user_id":this.id}, 'event':time + "getMeta"});
    this.corek.socket.on(time + "getMeta",(data,key)=> {
     for(let i of data){
      if(i.meta_key == "emails"){
        getJson = JSON.parse(i.meta_value);
        this.emailCorp1 = getJson.mail1;
        this.emailCorp2 = getJson.mail2;
        this.getJ = JSON.parse(i.meta_value);
       }
       if(i.meta_key == "identification"){
        getJson = JSON.parse(i.meta_value);
        console.log(getJson); 
        this.numLic = getJson.number;
        this.type = getJson.type;
        console.log(this.numLic, this.type);
        this.getJ1 = JSON.parse(i.meta_value);
      }
      if(i.meta_key == "community"){
        getJson = JSON.parse(i.meta_value);
        // this.communityValue = getJson;
        console.log(this.communityValue);
      }
     }
     loader.dismiss(); 
    });
  }

  updateUser(){
    if (this.getJ1.number != this.numberDoc){
      let nf = Date.now().toString()+"get_licences"+Math.random();
      this.corek.socket.emit('query',{'event':nf, 'querystring':"SELECT * FROM `wp_usermeta` WHERE `meta_key` LIKE 'identification'"});
      this.corek.socket.on(nf,(data,key)=> {
        for (let i in data){
          if(data[i].meta_value!="" && JSON.parse(data[i].meta_value).number == this.numberDoc){
            this.flag = 1; 
          }
        }
        if (this.flag != 1){
          this.getJ1.type = this.type;
          this.getJ1.number = this.numberDoc;
          this.corek.socket.emit('query',{'event':time + 'queryE1', 'querystring':"UPDATE wp_usermeta SET meta_value= '"+JSON.stringify(this.getJ1)+"' WHERE meta_key="+'"identification"'+' AND user_id="'+this.id+'"'});            
        }else{
          let toast = this.toastCtrl.create({
            message: 'Ese número de identificación ya existe. No se ha realizado el cambio de número',
            duration: 6000
          });
          toast.present();
        }
      });
    }
    
    let xdata = [""];
    if(this.aux == 1){
      if(this.auxName.length == 0){
        xdata = [""];
      }else{
        xdata = [];
      }
      for(let c of this.communitys ){
        for(let sel of this.auxName){
          if(sel == c.name && c.selected == true){
              console.log(c);
              xdata.push(c.name);
          }
        }
      }
      console.log(xdata);
    }else{
      console.log("vacio")
      this.communitys = [""];
    }
    const time = new Date().toISOString()+"name";
    let nf = new Date().toISOString+"commu"
    this.corek.socket.emit('add_user_meta',{'insert':{'user_id':this.id, 'meta_key':'community', 'meta_value':JSON.stringify(xdata), "unique":true}, 'key':'tel', 'event':nf});
    this.corek.socket.on(nf+"meta",(dat,key)=> {
      console.log(dat);
    });

    if (this.form.value.name == '') this.corek.socket.emit("update_user",{"set":{"user_url":this.showName},"condition":{"ID":this.id}, 'event':time + 'Name'});
    else if (this.form.value.lastName == '') this.corek.socket.emit("update_user",{"set":{"user_nicename":this.showLastName},"condition":{"ID":this.id}, 'event':time + 'Lname'});
    else if (this.form.value.phone == '') this.corek.socket.emit("update_user",{"set":{"user_login":this.phone},"condition":{"ID":this.id}, 'event':time + 'pho'});
   
    this.corek.socket.emit("update_user",{"set":{"user_url":this.form.value.name},"condition":{"ID":this.id}, 'event':time + 'name'});
    this.corek.socket.emit("update_user",{"set":{"user_nicename":this.form.value.lastName},"condition":{"ID":this.id}, 'event':time + 'lastName'});
    this.corek.socket.emit("update_user",{"set":{"user_login":this.form.value.phone},"condition":{"ID":this.id}, 'event':time + 'phone'});    
    
    if(this.form.value.email1!=''){
      if( !this.validate_email(this.form.value.email1) ){
        this.errorMail();
      }else{
        this.getJ.mail1 = this.form.value.email1;
        this.corek.socket.emit('query',{'event':time + 'query', 'querystring':"UPDATE wp_usermeta SET meta_value= '"+JSON.stringify(this.getJ)+"' WHERE meta_key="+'"emails"'+' AND user_id="'+this.id+'"'});
      }
    }
    if(this.form.value.email2!=''){
      if( !this.validate_email(this.form.value.email2) ){
        this.errorMail();
      }else{
        this.getJ.mail2 = this.form.value.email2;
        this.corek.socket.emit('query',{'event':time + 'queryE1', 'querystring':"UPDATE wp_usermeta SET meta_value= '"+JSON.stringify(this.getJ)+"' WHERE meta_key="+'"emails"'+' AND user_id="'+this.id+'"'});
      }
    }
    
  }

  save(){
    let loader = this.loadingCtrl.create({
      content: "Espere",
      duration: 15000
    });
    loader.present();
    if ( this.form.value.name == '' || this.form.value.lastName == '' || this.form.value.phone == '' || this.numberDoc == '') {
      let alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: 'No puedes tener el campo nombre, documento, apellido y telefono vacio',
        buttons: ['OK']
      });
      loader.dismiss();
      alert.present();
    } else {
      this.updateUser();
      // let toast = this.toastCtrl.create({
      //   message: 'Edición realizada con éxito',
      //   duration: 3000,
      //   position: 'bottom'
      // });
      
      // toast.onDidDismiss(() => {
      // });
      // toast.present();
      loader.dismiss();
      this.navCtrl.setRoot(ConfirmaciNCambiosPage);
      this.navCtrl.popToRoot();
    }
  }

  validate_email( email ) {
    var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email) ? true : false;
  }

  error(){
    let alert = this.alertCtrl.create({
      title: 'Error',
      message: 'Lo sentimos a ocurrido un error, intentalo de nuevo',
      buttons: [        
        {
          text: 'OK',
          handler: () => {              
          }
        }
      ]
    });
    alert.present();
  }

  errorMail(){
    let alert = this.alertCtrl.create({
      title: 'Error',
      message: 'Ingrese un correo valido',
      buttons: [        
        {
          text: 'OK',
          handler: () => {              
          }
        }
      ]
    });
    alert.present();
  }

  // is not being used ...

  // changePwd(){/
  //   if(this.pwd!=undefined || this.pwd!=''){
  //     if(this.pwd.length<8){
  //       let toast = this.toastCtrl.create({
  //         message: 'Contraseña demasiado corta, minimo ocho dígitos',
  //         duration: 3000,
  //         position: 'top'
  //       });
        
  //       toast.onDidDismiss(() => {
  //       });
  //       toast.present();
  //     }else{
  //       this.corek.socket.emit('change_password',{"token":this.md5,"user_pass":this.pwd});
  //       this.corek.socket.on("change_password", function(result,key){
  //         console.log(result);
  //       });
  //     }
  //   }
  // }
  
}
