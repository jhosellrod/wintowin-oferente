import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MiActualViajePage } from '../mi-actual-viaje/mi-actual-viaje';

@Component({
  selector: 'page-confirmaci-nmensaje',
  templateUrl: 'confirmaci-nmensaje.html'
})
export class ConfirmaciNMensajePage {

  constructor(public navCtrl: NavController) {}
  goToCrearUnViaje(){
    this.navCtrl.setRoot(MiActualViajePage);
    this.navCtrl.popToRoot();
  }
}
