import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { IniciaSesionPage } from '../inicia-sesion/inicia-sesion';

@Component({
  selector: 'page-confirmaci-ndocumentaci-nveh-culo',
  templateUrl: 'confirmaci-ndocumentaci-nveh-culo.html'
})
export class ConfirmaciNDocumentaciNVehCuloPage {

  constructor(public navCtrl: NavController) {
  }
  goToIniciaSesion(params){
    if (!params) params = {};
    this.navCtrl.push(IniciaSesionPage);
  }
}
