import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage'
import { CorekProvider } from '../../providers/corek/corek';

@Component({
  selector: 'page-mi-grupo',
  templateUrl: 'mi-grupo.html'
})
export class MiGrupoPage {

  id;
  code;
  shiw_spinner = true;
  heros = [];
  clients = [];
  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, private storage: Storage, private corek: CorekProvider) {

    this.storage.get('user_code').then((data)=>{
      this.code = data;
    });

    this.storage.get('ID').then((data)=>{
      this.id = data;
      this.getGroup();
    });
  }
  
  getGroup(){
    let loader = this.loadingCtrl.create({
      content: "Espere",
      duration: 15000
    });
    loader.present();
    let nf = Date.now().toString()+'createuser'+Math.random();
    this.corek.socket.emit('get_users', {'event': nf, 'condition':{'user_activation_key': this.code}});
    this.corek.socket.on(nf, (data, key)=>{
      console.log(data)
      for (let i in data){
        if (data[i].ID != this.id && data[i].user_status == 1){
          this.heros.push(data[i].user_url+" "+data[i].user_nicename);
        }else if(data[i].ID != this.id && data[i].user_status == 3 || data[i].user_status == 2 ){
          this.clients.push(data[i].user_url+" "+data[i].user_nicename);
        }
      }
      loader.dismiss();
    });
  }
}
