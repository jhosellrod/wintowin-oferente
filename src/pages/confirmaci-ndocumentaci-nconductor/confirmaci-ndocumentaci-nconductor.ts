import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConfirmaciNRegistroUsuarioConductorPage } from '../confirmaci-nregistro-usuario-conductor/confirmaci-nregistro-usuario-conductor';
import { DatosDelVehCuloPage } from '../datos-del-veh-culo/datos-del-veh-culo';
import { CorekProvider } from '../../providers/corek/corek'

@Component({
  selector: 'page-confirmaci-ndocumentaci-nconductor',
  templateUrl: 'confirmaci-ndocumentaci-nconductor.html'
})
export class ConfirmaciNDocumentaciNConductorPage {

  user_id; form: FormGroup; 
  public code_user;
  constructor(public formBuilder: FormBuilder, public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public alertCtrl: AlertController,private corek: CorekProvider) {
    this.form = formBuilder.group({
      code: ['', [Validators.required,Validators.minLength(8)]]
    });
    console.log(this.navParams.get('user_id'));
    this.user_id = this.navParams.get('user_id');
    this.code_user = this.navParams.get('code_user');
  }

  goToConfirmaciNRegistroUsuarioConductor(){
    let loading = this.loadingCtrl.create({
      content: "Espere por favor"
    });
    loading.present();
    let nf = Date.now().toString()+"ins"+Math.random();
    this.corek.socket.emit('query_post', { 'condition': {"post_type":"cars", "comment_status": this.form.value.code} , 'key': 'notificacion', 'event': nf});
    this.corek.socket.on(nf, (data1, key) => {
      console.log(data1)
      if (data1.length >= 1){
        let getWallet = Date.now()+'getWallet'+Math.random();
        this.corek.socket.emit('get_users', { 'condition': {"user_activation_key":data1[0].post_status}, 'event': getWallet});
        this.corek.socket.on(getWallet, (users) => {
          if (users.length > 0 && users.length < 4){
            let nf1 = Date.now().toString()+"update"+Math.random();
            this.corek.socket.emit('update_user',{'set':{'code_car':this.form.value.code, 'user_activation_key':data1[0].post_status, 'wallet':users[0].wallet},'condition':{'ID':this.user_id,},'event':nf1,});
            this.corek.socket.on(nf1,(data,key)=> {
              let nf5 = Date.now()+'UpdateWallet'+Math.random();
              this.corek.socket.emit('query',{'event':nf5, 'querystring':"UPDATE `zadmin_wintowin`.`wp_users` SET `wallet` = (`wp_users`.`wallet` + 100) WHERE `wp_users`.`user_activation_key` LIKE '"+data1[0].post_status+"'"});
              console.log(data)
              loading.dismiss();
              this.navCtrl.setRoot(ConfirmaciNRegistroUsuarioConductorPage);
              this.navCtrl.popToRoot();
            });
          }else{
            loading.dismiss();
            const alert = this.alertCtrl.create({
              title: 'Lo sentimos',
              subTitle: 'Los grupos pueden incluir máximo cuatro (04) miembros. El grupo al que desea ingresar esta lleno',
              buttons: ['OK']
            });
            alert.present();
          }
        });
        console.log(data1)
      }else{
        loading.dismiss();
        const alert = this.alertCtrl.create({
          title: 'Lo sentimos',
          subTitle: 'Ese codigo no esta registrado',
          buttons: ['OK']
        });
        alert.present();
      }
    });
  }

  uuidv4() {
    return 'xxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

  goToDatosDelVehCulo(){
    this.navCtrl.setRoot(DatosDelVehCuloPage, {user_id: this.user_id, code_user:this.code_user});
    this.navCtrl.popToRoot();
  }

}
