import { Component, Injectable } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, ToastController, Events} from 'ionic-angular';
import { VerUsuarioPage } from '../ver-usuario/ver-usuario';
import { BackgroundGeolocation, BackgroundGeolocationConfig, BackgroundGeolocationResponse } from '@ionic-native/background-geolocation/index';
import { ConfirmaciNReporteDeViajePage } from '../confirmaci-nreporte-de-viaje/confirmaci-nreporte-de-viaje';
import { Storage } from '@ionic/storage'
import { CorekProvider } from '../../providers/corek/corek';
import { ResponderMensajePage } from '../responder-mensaje/responder-mensaje';

@Injectable()
@Component({
  selector: 'page-estado-del-viaje',
  templateUrl: 'estado-del-viaje.html'
})
export class EstadoDelViajePage {

  id;
  code_user;
  trip_id;
  disabled = false;
  points;
  // asientos
  seats;
  //usuarios a considerar
  ids = [];
  wins = 0;
  postulates=[];
  //users in trip
  clients = [];
  coments = [];
  califications = [];
  public getIdUser: any;
  public Myid: any;
  clients_mails = [];
  user_email;
  user_name;
  lat;
  long;
  public pickUpPoints = [];
  public pickUpPointsId = [];
  x;
  public loading;

  constructor(public events: Events, private backgroundGeolocation: BackgroundGeolocation, public navCtrl: NavController,  public navParams: NavParams, public toastCtrl: ToastController ,public loadingCtrl: LoadingController, public alertCtrl:AlertController ,private storage: Storage ,private corek: CorekProvider) {
    
    this.events.subscribe('killBG', (name) =>{
      this.killBackGround();
    })

    this.storage.get('seats').then((key) => {
      this.seats = key;
    });

    this.storage.get('user_code').then((key) => {
      this.code_user = key;
    });

    this.trip_id = this.navParams.get('trip_id');

    this.storage.get('user_mail').then((key) => {
      this.user_email = key;
    });

    this.storage.get('user_name').then((key) => {
      this.user_name = key;
    });

    this.storage.get('user_id').then((id)=>{
      this.id = id;

      this.corek.socket.on('new_postulate'+id, (data) => {
        if(data.data.action == true){
          this.disabled = true;
          this.ids.push(data.data.meta_id);
          this.postulates.push(data.data);
          console.log(this.postulates);
        }else if(data.data.action == false){
          let i = 0;
          for (let postulate of this.postulates){
            if (postulate.user_id == data.data.user_id){
              this.postulates.splice(i, 1);
              console.log(this.postulates);
              if (this.postulates.length < 1){
                this.disabled = false;
                console.log(this.postulates);
              }
            }
            i++;
          }
        }
      });
    });
}

ionViewDidLoad(){
  this.loading = this.loadingCtrl.create({
    content: "Espere",
    duration: 15000
  });
  this.loading.present();
  this.BGgeolocation();
  this.storage.get('user_id').then((id)=>{
    this.id = id;
    console.log("a" + this.id);
    let n = Date.now().toString()+'cone'+Math.random();
    this.corek.ConnectCorekconfig(n);
    this.corek.socket.on(n, (dat, key)=>{
      this.getPostulates();
      });
    });
  }

  getPostulates(){
    let nf = Date.now().toString()+'test'+Math.random();
    if (this.seats != 0){
      console.log(this.trip_id);
      this.corek.socket.emit('field_name',{'field_name':'client','post_id':this.trip_id,'event':nf});
      this.corek.socket.on(nf, (data, key)=>{
        this.ids = [];
        this.postulates = [];
        for (let i in data){
          this.ids.push(data[i].meta_id);
          this.postulates.push(JSON.parse(data[i].meta_value));
        }
        if(this.postulates.length > 0){
          this.disabled = true;
        }
      });
    }
    this.getClients();
  }

  getClients(){
    console.log('obtner clientes')
    let nf1 = Date.now().toString()+'prueba'+Math.random();
    this.corek.socket.emit('query_post', { 'condition': {"post_type":"trips", "post_parent":0, "post_author": this.id} ,'key': 'notificacion', 'event': nf1});
    this.corek.socket.on(nf1, (data1, key) => {
      
      if(data1.length != 0){
        this.clients = [];
        this.trip_id = data1[0].ID;
        this.points = Math.round(parseFloat(data1[0].to_ping));
        this.seats = data1[0].menu_order;
        if (data1[0].post_name != ''){
          this.clients.push(JSON.parse(data1[0].post_name));
        }
        if (data1[0].post_content_filtered != ''){
          this.clients.push(JSON.parse(data1[0].post_content_filtered));
        }
        if (data1[0].guid != ''){
          this.clients.push(JSON.parse(data1[0].guid));
        }
        this.califications.length = this.clients.length;
        this.coments.length = this.clients.length;
        for ( let i of this.clients ) {
          this.wins += parseFloat(i.distance);
        }

        console.log(this.wins);
        

      }
      console.log(this.clients);
      
      this.pickPoint();
    });
  }

  pickPoint(){
    if(this.clients.length > 0){
      console.log(this.trip_id);
      // this.myCurrentPosition();
      for (let client of this.clients){
        let time = Date.now().toString()+Math.random();
        this.corek.socket.emit('query',{'event':time, 'querystring':"SELECT * FROM `wp_posts` WHERE `to_ping` LIKE '"+client.user_id+"' AND `post_author`="+this.trip_id+" AND `post_type` LIKE 'pickUpPoint'"});
        this.corek.socket.on(time, (data, key)=>{
          this.pickUpPoints = [];
          this.pickUpPoints.push(data[0].post_content);
          this.pickUpPointsId.push(data[0].ID);
          console.log(this.pickUpPoints);
          console.log(this.pickUpPointsId);
        });
      }
    }
    this.loading.dismiss();
  }

  pickUser(i){
    if (this.clients[i].status == 0){
      this.clients[i].status = 1
    }else{
      this.clients[i].status = 0;
    }
    console.log(this.clients)
  }

  goToVerUsuario(i){
    this.navCtrl.setRoot(VerUsuarioPage, {client: this.postulates[i], meta_id: this.ids[i], trip_id: this.trip_id});
    this.navCtrl.popToRoot();
  }

  goToEstadoDelViaje(){
    this.navCtrl.setRoot(EstadoDelViajePage);
    this.navCtrl.popToRoot();
  }

  sendMailHero(){
    let nf5 = Date.now().toString()+'send_mail_hero'+Math.random();
    let mail = '<div style="position:relative;min-width:320px;">'+
    '<div class="clearfix borderbox" style="z-index:1;min-height:535px;background-image:none;border-width:0px;border-color:#000000;background-color:transparent;padding-bottom:62px;width:100%;max-width:800px;margin-left:auto;margin-right:auto;">'+
    '<div class="clearfix colelem" style="z-index:2;background-color:#000000;position:relative;margin-right:-10000px;width:100%;">'+
    '<div class="grpelem"><div></div></div>'+
    '<div class="clearfix grpelem" style="z-index:4;min-height:17px;background-color:transparent;line-height:18px;color:#FFFFFF;font-size:15px;font-family:roboto, sans-serif;font-weight:300;position:relative;margin-right:-10000px;width:19.25%;left:7.13%;" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true">'+
    '<p>¡Hola '+this.user_name+'!</p></div></div>'+
    '<div class="colelem" style="z-index:3;height:133px;opacity:1;filter:alpha(opacity=100);margin-top:91px;position:relative;width:18.13%;margin-left:40.13%; background-size: contain;">'+
    '<img src="http://159.203.82.152/assets/logo.png" alt=""></div>'+
    '<div class="clearfix colelem" style="z-index:13;min-height:17px;background-color:transparent;line-height:22px;color:#000000;font-size:18px;font-family:roboto, sans-serif;font-weight:700;margin-top:78px;position:relative;width:46.13%;margin-left:26.13%;">'+
    '<p>¡HAS TERMINADO TU VIAJE!.</p></div>'+
    '<br><br>'+
    '<p>Distancia recorrida:'+this.wins+'</p><br>'+
    '<p>Wins obtenidos:'+this.wins+'</p><br>'+
    '<div class="clearfix colelem" style="z-index:17;min-height:17px;background-color:transparent;line-height:22px;color:#000000;font-size:18px;font-family:roboto, sans-serif;font-weight:700;margin-top:77px;position:relative;width:46.13%;margin-left:26.13%;" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true">'+
    '<p>Gracias por ayudar a transportar usuarios. En la App móvil podrás verificar los wins obtenidos por tus viajes en la pantalla “wallet”..</p></div>'+
    '<div class="clearfix colelem" style="background-color:#000000; height:48px; z-index:8;margin-top:111px;width:100%;">'+
    '<div class="grpelem" style="display:inline;float:left; z-index:8;background-color:#000000;position:relative;margin-right:-10000px;width:100%;">'+
    '<div class="fluid_height_spacer"></div></div><div class="clearfix grpelem" style="height: 17px; display:inline;float:left;z-index:9;min-height:17px;background-color:transparent;line-height:18px;color:#FFFFFF;font-size:15px;font-family:roboto, sans-serif;font-weight:300;position:relative;margin-right:-10000px;width:10%;left:43%; ">'+
    '<p>WIN to WIN</p></div></div>'+
    '<div class="clearfix colelem" style="z-index:21;min-height:17px;background-color:transparent;line-height:14px;color:#000000;font-size:12px;font-family:roboto, sans-serif;font-weight:300;margin-top:9px;position:relative;width:45.88%;margin-left:28.38%;" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true">'+
    '<p>¿Tienes dudas? Envíanos un mensaje a soporte@wintowin.co</p></div><div class="verticalspacer" data-offset-top="600" data-content-above-spacer="599" data-content-below-spacer="61" data-sizePolicy="fixed" data-pintopage="page_fixedLeft"></div>'
    +'</div></div>';
    this.corek.socket.emit('confsmtp',{'key':'confsmtp','token':8,'event':nf5});
    this.corek.socket.on(nf5, (data, key)=>{
      if(key == 'confsmtp'){
        console.log('validar aqui');
        this.corek.socket.emit('sendemail',{'event':event,'key':'sendemail',
        'from':'"WinToWin" <noreply@wintowin.com.co>',
        'to':this.user_email,
        'subject':'Viaje finalizado',
        'html':mail
        });    
        this.corek.socket.on(event, (data , key)=>{});
      }
    });
  }

  sendMail(nf, name, email){
    console.log(nf, name, email);
    let mail = '<div style="position:relative;min-width:320px;">'+
    '<div class="clearfix borderbox" style="z-index:1;min-height:535px;background-image:none;border-width:0px;border-color:#000000;background-color:transparent;padding-bottom:62px;width:100%;max-width:800px;margin-left:auto;margin-right:auto;">'+
    '<div class="clearfix colelem" style="z-index:2;background-color:#000000;position:relative;margin-right:-10000px;width:100%;">'+
    '<div class="grpelem"><div></div></div>'+
    '<div class="clearfix grpelem" style="z-index:4;min-height:17px;background-color:transparent;line-height:18px;color:#FFFFFF;font-size:15px;font-family:roboto, sans-serif;font-weight:300;position:relative;margin-right:-10000px;width:19.25%;left:7.13%;" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true">'+
    '<p>¡Hola '+name+'!</p></div></div>'+
    '<div class="colelem" style="z-index:3;height:133px;opacity:1;filter:alpha(opacity=100);margin-top:91px;position:relative;width:18.13%;margin-left:40.13%; background-size: contain;">'+
    '<img src="http://159.203.82.152/assets/logo.png" alt=""></div>'+
    '<div class="clearfix colelem" style="z-index:13;min-height:17px;background-color:transparent;line-height:22px;color:#000000;font-size:18px;font-family:roboto, sans-serif;font-weight:700;margin-top:78px;position:relative;width:46.13%;margin-left:26.13%;">'+
    '<p>¡HAS TERMINADO TU VIAJE!.</p></div>'+
    '<div class="clearfix colelem" style="z-index:17;min-height:17px;background-color:transparent;line-height:22px;color:#000000;font-size:18px;font-family:roboto, sans-serif;font-weight:700;margin-top:77px;position:relative;width:46.13%;margin-left:26.13%;" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true">'+
    '<p>Distancia recorrida: '+this.wins+'</p><br>'+
    '<p>Wins obtenidos: '+this.wins+'</p><br>'+
    '<p>Esperamos que hayas disfrutado de tu viaje. El sistema descontará los wins necesarios por el recorrido.</p></div>'+
    '<div class="clearfix colelem" style="background-color:#000000; height:48px; z-index:8;margin-top:111px;width:100%;">'+
    '<div class="grpelem" style="display:inline;float:left; z-index:8;background-color:#000000;position:relative;margin-right:-10000px;width:100%;">'+
    '<div class="fluid_height_spacer"></div></div><div class="clearfix grpelem" style="height: 17px; display:inline;float:left;z-index:9;min-height:17px;background-color:transparent;line-height:18px;color:#FFFFFF;font-size:15px;font-family:roboto, sans-serif;font-weight:300;position:relative;margin-right:-10000px;width:10%;left:43%; ">'+
    '<p>WIN to WIN</p></div></div>'+
    '<div class="clearfix colelem" style="z-index:21;min-height:17px;background-color:transparent;line-height:14px;color:#000000;font-size:12px;font-family:roboto, sans-serif;font-weight:300;margin-top:9px;position:relative;width:45.88%;margin-left:28.38%;" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true">'+
    '<p>¿Tienes dudas? Envíanos un mensaje a soporte@wintowin.co</p></div><div class="verticalspacer" data-offset-top="600" data-content-above-spacer="599" data-content-below-spacer="61" data-sizePolicy="fixed" data-pintopage="page_fixedLeft"></div>'
    +'</div></div>';
    this.corek.socket.emit('confsmtp',{'key':'confsmtp','token':8,'event':nf});
    this.corek.socket.on(nf, (data, key)=>{
      console.log(data);
      console.log(data.key);
      if(key == 'confsmtp'){
        console.log('validar aqui');
        this.corek.socket.emit('sendemail',{'event':event,'key':'sendemail',
        'from':'"WinToWin" <noreply@wintowin.com.co>',
        'to':email,
        'subject':'Viaje finalizado',
        'html':mail
        });    
        this.corek.socket.on(event, (data , key)=>{
        console.log(data);
        console.log(key);
        });
      }
    });
  }

  sendNotification(){
    let n = Date.now().toString()+'cone'+Math.random();
    this.corek.ConnectCorekconfig2(n);
    this.corek.socket.on(n, (dat, key)=>{
      if(dat.conf == true){
        for (let client of this.clients){
          var not = {
            notification: {
              title: 'Ha terminado el viaje', 
              body:"Recuerda calificar al usuario",
            },
            android: {
              notification: {
                color: '#ebc041',
                sound:'default',
              },
            },
            'topic':'notification'+client.user_id,
          };
          this.corek.socket.emit('emitnotification', {notification:not});
          this.corek.socket.emit('newpost',{"data": {accepted:false, reservacion:false, calification:true}, event:'listen'+client.user_id});
        }
      }
    });
  }

  goToConfirmaciNReporteDeViaje(){
    let loading = this.loadingCtrl.create({
      content: "Espere",
      duration: 15000
    });
    loading.present();
    if (this.clients.length == 0){
      let toast = this.toastCtrl.create({
        message: 'Aún no aceptas ningún pasajero',
        duration: 6000
      });
      loading.dismiss();
      toast.present();
    }else{
      let r=1;
      let nf2 = Date.now().toString()+'send_mail'+Math.random();
      let nf7 = Date.now().toString()+'add'+Math.random();
      const confirm = this.alertCtrl.create({
        title: 'Estas apunto de enviar el reporte del viaje',
        message: '¿Seguro que deseas continuar? En caso de no hacer algún comentario al usuario por calificar su calificación no se enviará',
        buttons: [
          {
            text: 'Continuar',
            handler: () => {
              this.updatePostulates();
              this.killBackGround();
              loading.dismiss();
              let i=0;
              let j=1;
              // Variable a sumar al hero
              let totalWins = 0;
              let distance: any = 0;
              for (let client of this.clients){
                distance = client.distance;
                r++;
                nf2+=r;
                nf7+=r;
                if (client.status == 1){
                  console.log('usuario recogido');
                  if (client.clients.length <= 6){
                    for (let code of client.clients){
                      // Restar puntos
                      let nf6 = Date.now().toString()+'discount'+Math.random();
                      this.corek.socket.emit('query',{'event':nf6, 'querystring':"UPDATE `zadmin_wintowin`.`wp_users` SET `wallet` = (`wp_users`.`wallet`- "+Math.round(client.distance)+") WHERE `wp_users`.`user_activation_key` LIKE '"+code+"'"});
                      totalWins = totalWins + Math.round(client.distance);
                      distance += Math.round(client.distance);
                    }
                  }else{
                    // Restar puntos
                    let nf6 = Date.now().toString()+'discount'+Math.random();
                    this.corek.socket.emit('query',{'event':nf6, 'querystring':"UPDATE `zadmin_wintowin`.`wp_users` SET `wallet` = (`wp_users`.`wallet`- "+Math.round(client.distance)+") WHERE `wp_users`.`user_activation_key` LIKE '"+client.clients+"'"});
                    totalWins = totalWins + Math.round(client.distance);
                    distance += Math.round(client.distance);
                  }
                  
                }
                this.updateUser(client);
                this.upLoadPick(this.pickUpPointsId[i]);
                this.sendMail(nf2, client.name, client.user_mail);
                this.sendCalification(client.user_id, this.califications[i], this.coments[i]);
                // Validacion de que se recorrio todo el arreglo
                console.log(i, this.clients.length)
                if (j == this.clients.length){
                  console.log('Suma de puntos')
                  // Sumar puntos
                  this.corek.socket.emit('query',{'event':nf7, 'querystring':"UPDATE `zadmin_wintowin`.`wp_users` SET `wallet` = (`wp_users`.`wallet`+ "+totalWins+") WHERE `wp_users`.`user_activation_key` LIKE '"+this.code_user+"'"});
                }
                i++;
                j++;
              }
              //Actualizar status de viaje
              let nf3 = Date.now().toString()+"ins"+Math.random();
              this.corek.socket.emit('query',{'event':nf3, 'querystring':"UPDATE `zadmin_wintowin`.`wp_posts` SET `post_parent` = '1' WHERE `wp_posts`.`ID` = "+this.trip_id});
              this.corek.socket.on(nf3, (status)=>{
                console.log(status);
                // Eliminar postulados
                let nf4 = Date.now().toString()+"insi"+Math.random();
                this.corek.socket.emit('query',{'event':nf4, 'querystring':"DELETE FROM wp_postmeta WHERE wp_postmeta.post_id = "+this.trip_id});
                this.corek.socket.on(nf4, (deleteP)=>{
                  console.log(deleteP)
                  this.sendNotification();
                  loading.dismiss();
                  
                  console.log(totalWins);
                  console.log(distance);
                  this.navCtrl.setRoot(ConfirmaciNReporteDeViajePage, {data:this.wins});
                  this.navCtrl.popToRoot();
                });
              });
            }
          },
          {
            text: 'Cancelar',
            handler: () => {
              loading.dismiss();
            }
          }
        ]
      });
      confirm.present();
    }
  }

  updatePostulates(){
    // Update status user
    for (let postulate of this.postulates){
      let updatePostulate = Date.now()+'updateePostulate'+Math.random();
      this.corek.socket.emit('update_user',{'set':{'id_hero':null, 'id_trip':null},'condition':{'ID':postulate.user_id}, 'event':updatePostulate});
    }
  }

  updateUser(client){
    console.log(client)
    let updateClient = Date.now()+'updatee'+Math.random();
    this.corek.socket.emit('update_user',{'set':{'calification':1},'condition':{'ID':client.user_id}, 'event':updateClient});
  }

  sendCalification(ID, calification, coment){
    if (coment){
      const time = Date.now().toString()+Math.random();
      this.corek.socket.emit('query',{'event':time, 'querystring':"SELECT * FROM `wp_posts` WHERE `post_author` = "+ this.id+" AND `to_ping` ="+ID+" AND `post_type` LIKE 'calification'"});
      this.corek.socket.on(time, (data1, key) => {
        console.log(data1)
        if(data1.length > 0){
          let sumNumberOfCali = parseInt(data1[0].post_excerpt) + 1;
          let sumCali = 0;
          if(calification){
            sumCali = calification+parseInt(data1[0].post_title);
          }else{
            sumCali = 1+parseInt(data1[0].post_title);
          }
          let totalCali = sumCali.toString();
          let totalNumberOfCali = sumNumberOfCali.toString();
          let updates = Date.now().toString()+Math.random();
          this.corek.socket.emit("update_post",{"set":{'post_content':coment, 'post_title':totalCali, 'post_excerpt':totalNumberOfCali},"condition":{"post_author":this.id, "to_ping":ID}, 'event':updates});
          this.corek.socket.on(updates, (update)=>{
            console.log(update);
          })
        }else{
          let inserts = Date.now().toString()+Math.random();
          if(calification){
            this.corek.socket.emit('insert_post',{'condition':{'post_type':'calification', 'post_author': this.id, 'post_content':coment,'post_title':calification, 'post_excerpt':1, 'to_ping': ID},'event':inserts});
          }else{
            this.corek.socket.emit('insert_post',{'condition':{'post_type':'calification', 'post_author': this.id, 'post_content':coment,'post_title':1, 'post_excerpt':1, 'to_ping': ID},'event':inserts});
          }
          this.corek.socket.on(inserts, (insert)=>{
            console.log(insert);
          });
        }
      })
    }
  }
      
  seeUser(i){
    this.navCtrl.setRoot(VerUsuarioPage, {client: this.clients[i], meta_id: this.ids[i],flag:true,trip_id:this.trip_id});
    this.navCtrl.popToRoot();
  }

  message(){
    console.log(this.getIdUser.user_id);
    this.navCtrl.setRoot(ResponderMensajePage, {UserId:this.getIdUser.user_id});
    this.navCtrl.popToRoot();
  }
  
  updateCodeGroup(time,idUser){
    this.corek.socket.emit('query',{'event':time, 'querystring':"UPDATE `zadmin_wintowin`.`wp_users` SET `user_activation_key` = '' WHERE `wp_users`.`ID` = "+idUser});
  }

  test(){
    let time = Date.now().toString()+'DB'+Math.random();
    let nf1 = Date.now().toString()+'position'+this.lat+Math.random();
    // Insercion en DB
    

  }

  BGgeolocation(){
    let time = Date.now().toString()+Math.random();
    console.log('hola');
    const config: BackgroundGeolocationConfig = {
      desiredAccuracy: 10,
      stationaryRadius: 5,
      distanceFilter: 5,
      debug: false, //  enable this hear sounds for background-geolocation life-cycle.
      stopOnTerminate: false, // enable this to clear background location settings when the app terminates
      startOnBoot: true,
      activitiesInterval: 5000,
      interval: 10000,
      fastestInterval: 5000,
      startForeground: true,
      notificationTitle: 'WinToWin HERO',
      notificationText: 'Compartiendo ubicación',
    };
    this.backgroundGeolocation.configure(config)
    .subscribe((location: BackgroundGeolocationResponse) => {
      this.lat = location.latitude + '';
      this.long = location.longitude + '';
      let nf1 = Date.now().toString()+'position'+this.lat+Math.random();
      let positionHero = {'latitud':this.lat, 'longitud':this.long};
      // Envio por socket 
      this.corek.socket.emit('newpost',{"data":positionHero, event:'position_hero'+this.trip_id});
      // Insercion en DB
      this.corek.socket.emit('query',{'event':nf1, 'querystring':"UPDATE wp_posts SET post_mime_type= '"+JSON.stringify(positionHero)+"' WHERE post_type="+'"trips"'+' AND ID="'+this.trip_id+'"'});
      this.corek.socket.emit('query_post', { 'condition': {"post_type":'positionHero', 'post_author':this.id, 'post_password': this.trip_id} ,'key': 'notificon', 'event': time + 'query230'});
      this.corek.socket.on(time + 'query230', (data)=>{
        if(data.length > 0){
          console.log('mas de 0')  
          this.corek.socket.emit('query',{'event':time + 'upda', 'querystring':"UPDATE `zadmin_wintowin`.`wp_posts` SET `post_content` = '"+this.lat+"', `post_title` = '"+this.long+"' WHERE `wp_posts`.`post_type` = 'positionHero' AND `wp_posts`.`post_author` = "+this.id+" AND `wp_posts`.`post_password` = " +this.trip_id});
        }else if (data.length == 0){
          this.corek.socket.emit('insert_post',{'condition':{'post_type':'positionHero', 'post_author':this.id,'post_content':this.lat,'post_title':this.long, 'post_password': this.trip_id, 'comment_status':'open'},'event':time +'position'});    
          this.corek.socket.on(time + 'position', (data)=>{
            console.log('aqui estoy '+data);
          });
        }
      });
    });
    this.backgroundGeolocation.start();    
  }

  killBackGround(){
    this.backgroundGeolocation.stop();
  }

  upLoadPick(id) {
    console.log(id);
    const time = Date.now().toString()+Math.random();
    this.corek.socket.emit('query',{'event':time+'p', 'querystring':"UPDATE `zadmin_wintowin`.`wp_posts` SET `post_title` = 0 WHERE `wp_posts`.`ID` ="+id});
    this.corek.socket.on(time+'p',(data)=>{
      console.log(data);
    })
  }


  ionViewWillLeave(){
    console.log('--------------------------------------');
    console.log('Will leave');
  }

  ionViewDidLeave(){
    console.log('--------------------------------------');
    console.log('Did Leave');
  }
}

