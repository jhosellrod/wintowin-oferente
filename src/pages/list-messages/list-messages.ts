import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController} from 'ionic-angular';
import { CorekProvider } from '../../providers/corek/corek'
import { Storage } from '@ionic/storage';
import { MensajeRecibidoPage } from '../mensaje-recibido/mensaje-recibido';

/**
 * Generated class for the ListMessagesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-list-messages',
  templateUrl: 'list-messages.html',
})
export class ListMessagesPage {
public id: any;
trip_id;
public items = [];
  constructor(public loadingCtrl: LoadingController, private storage: Storage, private corek: CorekProvider, public navCtrl: NavController, public navParams: NavParams) {
    this.storage.get('ID').then((val) => {
      this.id = val;
      this.showAuthorMessage();
    });
    this.trip_id = this.navParams.get('trip_id');
  }

  showAuthorMessage(){
    let loading = this.loadingCtrl.create({
      content: "Espere",
      duration: 10000
    });
    loading.present();
    const time = Date.now()+Math.random();
    this.corek.socket.emit('query',{'event':time + 'query1', 'querystring':"SELECT `wp_posts`.`ID`, `wp_posts`.`post_title`, `wp_posts`.`post_author`, `wp_posts`.`post_content`, `wp_users`.`display_name` FROM `wp_posts` INNER JOIN `wp_users` ON `wp_users`.`ID` = `wp_posts`.`post_author` WHERE `wp_posts`.`post_type` LIKE 'listMessage' AND `wp_posts`.`post_excerpt` = "+this.id+" AND `wp_posts`.`post_parent` = "+this.trip_id+" GROUP BY `post_title`"});
    this.corek.socket.on(time + 'query1', (datas, key) => {
      console.log(datas)
      for (let data of datas){  
        this.items.push({'ID':data.ID, 'id_user':data.post_author,'name':data.post_title,'photo':this.corek.ipServe()+data.display_name, 'message':data.post_content});
      }
    });
    loading.dismiss();
  }

  showMessage(chat){
    this.navCtrl.setRoot(MensajeRecibidoPage, {'dataChatUser': chat, 'trip_id':this.trip_id});
    this.navCtrl.popToRoot();
  }
}