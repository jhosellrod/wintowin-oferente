import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListMessagesPage } from './list-messages';

@NgModule({
  declarations: [
    ListMessagesPage,
  ],
  imports: [
    IonicPageModule.forChild(ListMessagesPage),
  ],
})
export class ListMessagesPageModule {}
