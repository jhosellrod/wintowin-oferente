import { Component } from '@angular/core';
import { NavController, ToastController, NavParams, LoadingController} from 'ionic-angular';
import { CorekProvider } from '../../providers/corek/corek';
import { Storage } from '@ionic/storage';
import { ConfirmaciNMensajePage } from '../confirmaci-nmensaje/confirmaci-nmensaje';

@Component({
  selector: 'page-responder-mensaje',
  templateUrl: 'responder-mensaje.html'
})
export class ResponderMensajePage {
public id: any;
trip_id;
public UserId: any;
public message: any;
public nameR: any;
public nameS: any;
  constructor(private storage: Storage, public navParams: NavParams, private corek: CorekProvider,public toast: ToastController, public navCtrl: NavController, public loadingCtrl: LoadingController) {
    this.UserId = this.navParams.get('idSendMessage');
    console.log(this.navParams.get('idSendMessage'))
    this.trip_id = (this.navParams.get('trip_id'));
    this.storage.get('user_name').then((val) => {
      this.nameS = val;
    });
    this.storage.get('ID').then((val) => {
      this.id = val;
    });
  }

  goToEstadoDelViaje(){
    let loading = this.loadingCtrl.create({
      content: 'Espere', 
      duration: 15000
    });
    loading.present();
    let toast = this.toast.create({
      message: 'Mensaje enviado.',
      duration: 6000
    });
    let now = new Date();
    let date = now.getFullYear()+'-'+(now.getMonth()+1)+'-'+now.getDate()+' '+now.getHours()+':'+now.getMinutes()+':'+now.getSeconds();
    let time = Date.now().toString()+Math.random();
    this.nameR = this.UserId.name+" "+this.UserId.lastName
    this.corek.socket.emit('insert_post',{'condition':{'post_type':'listMessageHero','post_status':'publish', 'post_author':this.id, 'post_excerpt':this.UserId.id_user, 'post_parent':this.trip_id, 'post_content':this.message, 'post_content_filtered':this.nameR,'post_title':this.nameS, 'post_date':date},'event':time +'inserListMedssage'});
    this.corek.socket.on(time +'inserListMedssage', (result)=>{
      this.corek.ConnectCorekconfig2(time+'conecction');
      this.corek.socket.on(time+'conecction', (dat, key)=>{
        toast.present();
        loading.dismiss();
        if (dat.conf == true){
          var not = {
            notification: {
              title: 'Mensaje del HERO '+this.nameS, 
              body:this.message,
            },
            android: {
              notification: {
                color: '#ebc041',
                sound:'default',
              },
            },
            'topic':'notification'+this.UserId.id_user,
          };
          // Envio de notificacion
          this.corek.socket.emit('emitnotification', {notification:not});
          this.navCtrl.setRoot(ConfirmaciNMensajePage);
          this.navCtrl.popToRoot();
        }
      });
    });
    console.log(this.message);
  }
}