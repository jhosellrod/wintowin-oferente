import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { CrearUnViajePage } from '../crear-un-viaje/crear-un-viaje';

@Component({
  selector: 'page-confirmaci-ncambio-de-ruta',
  templateUrl: 'confirmaci-ncambio-de-ruta.html'
})
export class ConfirmaciNCambioDeRutaPage {

  constructor(public navCtrl: NavController) {}
  goToCrearUnViaje(){
    this.navCtrl.setRoot(CrearUnViajePage);
  }
}
