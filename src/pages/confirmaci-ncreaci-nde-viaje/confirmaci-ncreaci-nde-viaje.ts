import { Component } from '@angular/core';
import { NavController} from 'ionic-angular';
import { MiActualViajePage } from '../mi-actual-viaje/mi-actual-viaje';

@Component({
  selector: 'page-confirmaci-ncreaci-nde-viaje',
  templateUrl: 'confirmaci-ncreaci-nde-viaje.html'
})
export class ConfirmaciNCreaciNDeViajePage {

  trip_id;
  constructor(public navCtrl: NavController) {}

  goToEstadoDelViaje(){
    this.navCtrl.setRoot(MiActualViajePage);
    this.navCtrl.popToRoot();
  }
}
