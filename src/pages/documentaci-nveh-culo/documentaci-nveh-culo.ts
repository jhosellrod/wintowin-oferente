import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, ActionSheetController} from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { CorekProvider } from '../../providers/corek/corek';
import { ConfirmaciNDocumentaciNVehCuloPage } from '../confirmaci-ndocumentaci-nveh-culo/confirmaci-ndocumentaci-nveh-culo';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import moment from 'moment';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';

@Component({
  selector: 'page-documentaci-nveh-culo',
  templateUrl: 'documentaci-nveh-culo.html'
})
export class DocumentaciNVehCuloPage {

  car_data;
  form: FormGroup;
  min;
  user_id;
  public code_user: any;
  constructor(private transfer: FileTransfer, private file: File, public formBuilder: FormBuilder, public navCtrl: NavController, public navParams: NavParams ,public loadingCtrl: LoadingController , private corek: CorekProvider, public alertCtrl: AlertController, private camera: Camera , public actionSheetCtrl: ActionSheetController) {
    this.form = formBuilder.group({
      soat_date: ['', [Validators.required]],
      policy_date: ['', [Validators.required]],
      rtm_date: ['', [Validators.required]],
    });
    this.user_id = this.navParams.get('user_id');
    this.car_data = this.navParams.get('car_data');
    this.code_user = this.navParams.get('code_user');
    console.log(this.code_user);
    this.min = moment().format("YYYY-MM-DD");
  }
  soat_img=''; soat_back=''; rtm_img=''; property_img=''; property_imgB=''; policy_img=''; code_car;

  goToConfirmaciNDocumentaciNVehCulo(){
    let loading = this.loadingCtrl.create({
      content: "Espere por favor"
    });
    loading.present();
    //insercion del vehiculo
    this.code_car = this.uuidv4();
    //consulta si existe el codigo
    let nf3 = Date.now().toString()+"ins"+Math.random();
    this.corek.socket.emit('query_post', { 'condition': {"post_type":"cars", "comment_status": this.code_car} , 'key': 'notificacion', 'event': nf3});
    this.corek.socket.on(nf3, (data1, key) => {
      if (data1.length >= 1){
        this.code_car = this.uuidv4();
      }else if (this.soat_img == null || this.soat_img == ''){
        const alert = this.alertCtrl.create({
          title: 'Es necesario el SOAT',
          subTitle: 'Necesitamos que cargues el SOAT para validar tus datos!',
          buttons: ['OK']
        });
        loading.dismiss();
        alert.present();
      }else if(this.property_img == null || this.property_img == ''){
        const alert = this.alertCtrl.create({
          title: 'Es necesaria la tarjeta de propiedad',
          subTitle: 'Necesitamos que cargues la tarjeta de propiedad para validar tus datos!',
          buttons: ['OK']
        });
        loading.dismiss();
        alert.present();
      }else{
        //insercion de los datos
        let nf4 = Date.now().toString()+"cars"+Math.random();
        this.corek.socket.emit('insert_post',{'condition':{'post_type':'cars',
            'post_content_filtered': this.car_data.registration,
            'post_date':this.form.value.soat_date,
            'post_date_gmt':this.form.value.rtm_date,
            'post_content': this.soat_img,
            'post_title': this.car_data.place,
            'post_excerpt': this.car_data.model,
            'post_mime_type': this.property_img,
            'property_back': this.property_imgB,
            'ping_status': this.soat_back,
            'post_status': this.code_user,
            'comment_status': this.code_car, 
            'guid': this.user_id,
            'post_modified': this.form.value.policy_date,
            'post_modified_gmt': new Date(),
            'post_password':this.rtm_img,
            'post_name': this.policy_img,
            'to_ping': this.car_data.line,
            'pinged': this.car_data.brand,
          },"event":nf4});
        this.corek.socket.on(nf4, (data3)=>{
          let nf1 = Date.now().toString()+"update"+Math.random();
          this.corek.socket.emit('update_user',{'set':{'code_car':this.code_car},'condition':{'ID':this.user_id,},'event':nf1});
          this.corek.socket.on(nf1,(data,key)=> {
            loading.dismiss();
            this.navCtrl.setRoot(ConfirmaciNDocumentaciNVehCuloPage);
            this.navCtrl.popToRoot();
          });
        });
      }
    });
  }

  uuidv4() {
    return 'xxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

  upload_soat(){
    let loading = this.loadingCtrl.create({
      content: "Espere",
      duration: 5000
    });
    let actionSheet = this.actionSheetCtrl.create({
      title: 'SOAT',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Destructive clicked');
          }
        },{
          text: 'Camara',
          handler: () => {
            console.log('Cam clicked');
            const options: CameraOptions = {
              quality: 70,
              sourceType: this.camera.PictureSourceType.CAMERA,
              destinationType: this.camera.DestinationType.NATIVE_URI,
              encodingType: this.camera.EncodingType.JPEG,
              mediaType: this.camera.MediaType.PICTURE ,
              correctOrientation : false
              // targetWidth: 200,
              // targetHeight: 200,
            }
            this.camera.getPicture(options).then((imageData) => {
              let d = new Date();
              let st = d.getDate() +"-"+ d.getMonth() +"-"+ d.getFullYear() +"-"+ d.getHours() +"-"+ d.getMinutes() +"-"+ d.getSeconds() +"-"+ d.getMilliseconds();
              const namefile =  st + "-"+ this.user_id + "-"+ 'cam.jpg' ; 
              let options1: FileUploadOptions = {
                fileKey: 'file',
                fileName: namefile,
                headers: {}
              }
              const fileTransfer: FileTransferObject = this.transfer.create();
              loading.present();
              fileTransfer.upload(imageData, 'http://159.203.82.152/custom-upload.php', options1).then((data) => {
                this.soat_img = namefile;
                }, (err) => {
                  alert('Error. Intente de nuevo')
                });
              }, (err) => {
                // Handle error
            });
          }
        },{
          text: 'Galeria',
          handler: () => {
            console.log('Gal clicked');
            const options: CameraOptions = {
              quality: 100,
              sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
              destinationType: this.camera.DestinationType.FILE_URI,
              encodingType: this.camera.EncodingType.JPEG,
              mediaType: this.camera.MediaType.PICTURE,
              correctOrientation : false,
              // targetWidth: 200,
              // targetHeight: 200,
            }
            this.camera.getPicture(options).then((imageData) => {
              let d = new Date();
              let st = d.getDate() +"-"+ d.getMonth() +"-"+ d.getFullYear() +"-"+ d.getHours() +"-"+ d.getMinutes() +"-"+ d.getSeconds() +"-"+ d.getMilliseconds();
              const namefile =  st + "-"+ this.user_id+ "-"+ 'gal.jpg' ; 
              let options1: FileUploadOptions = {
                fileKey: 'file',
                fileName: namefile,
                headers: {}
              }
              const fileTransfer: FileTransferObject = this.transfer.create();
              loading.present();
              fileTransfer.upload(imageData, 'http://159.203.82.152/custom-upload.php', options1).then((data) => {
                this.soat_img = namefile;
                }, (err) => {
                  alert('Error. Intente de nuevo')
                });
            });
        }
        }
      ]
    });
    actionSheet.present();
  }

  upload_soatBack(){
    let loading = this.loadingCtrl.create({
      content: "Espere",
      duration: 5000
    });
    let actionSheet = this.actionSheetCtrl.create({
      title: 'SOAT',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Destructive clicked');
          }
        },{
          text: 'Camara',
          handler: () => {
            console.log('Cam clicked');
            const options: CameraOptions = {
              quality: 70,
              sourceType: this.camera.PictureSourceType.CAMERA,
              destinationType: this.camera.DestinationType.NATIVE_URI,
              encodingType: this.camera.EncodingType.JPEG,
              mediaType: this.camera.MediaType.PICTURE ,
              correctOrientation : false
              // targetWidth: 200,
              // targetHeight: 200,
            }
            this.camera.getPicture(options).then((imageData) => {
              let d = new Date();
              let st = d.getDate() +"-"+ d.getMonth() +"-"+ d.getFullYear() +"-"+ d.getHours() +"-"+ d.getMinutes() +"-"+ d.getSeconds() +"-"+ d.getMilliseconds();
              const namefile =  st + "-"+ this.user_id+ "-"+ 'cam.jpg' ; 
              let options1: FileUploadOptions = {
                fileKey: 'file',
                fileName: namefile,
                headers: {}
              }
              const fileTransfer: FileTransferObject = this.transfer.create();
              loading.present();
              fileTransfer.upload(imageData, 'http://159.203.82.152/custom-upload.php', options1).then((data) => {
                this.soat_back = namefile;
                }, (err) => {
                  alert('Error. Intente de nuevo')
                });
              }, (err) => {
                // Handle error
            });
          }
        },{
          text: 'Galeria',
          handler: () => {
            console.log('Gal clicked');
            const options: CameraOptions = {
              quality: 100,
              sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
              destinationType: this.camera.DestinationType.FILE_URI,
              encodingType: this.camera.EncodingType.JPEG,
              mediaType: this.camera.MediaType.PICTURE,
              correctOrientation : false,
              // targetWidth: 200,
              // targetHeight: 200,
            }
            this.camera.getPicture(options).then((imageData) => {
              let d = new Date();
              let st = d.getDate() +"-"+ d.getMonth() +"-"+ d.getFullYear() +"-"+ d.getHours() +"-"+ d.getMinutes() +"-"+ d.getSeconds() +"-"+ d.getMilliseconds();
              const namefile =  st + "-"+ this.user_id + "-"+ 'gal.jpg' ; 
              let options1: FileUploadOptions = {
                fileKey: 'file',
                fileName: namefile,
                headers: {}
              }
              const fileTransfer: FileTransferObject = this.transfer.create();
              loading.present();
              fileTransfer.upload(imageData, 'http://159.203.82.152/custom-upload.php', options1).then((data) => {
                this.soat_back = namefile;
                }, (err) => {
                  alert('Error. Intente de nuevo')
                });
            });
          }
        }
      ]
    });
    actionSheet.present();
  }

  upload_rtm(){
    let loading = this.loadingCtrl.create({
      content: "Espere",
      duration: 5000
    });
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Revision Tecnomecanica',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Destructive clicked');
          }
        },{
          text: 'Camara',
          handler: () => {
            console.log('Cam clicked');
            const options: CameraOptions = {
              quality: 70,
              sourceType: this.camera.PictureSourceType.CAMERA,
              destinationType: this.camera.DestinationType.NATIVE_URI,
              encodingType: this.camera.EncodingType.JPEG,
              mediaType: this.camera.MediaType.PICTURE ,
              correctOrientation : false
              // targetWidth: 200,
              // targetHeight: 200,
            }
            this.camera.getPicture(options).then((imageData) => {
              let d = new Date();
              let st = d.getDate() +"-"+ d.getMonth() +"-"+ d.getFullYear() +"-"+ d.getHours() +"-"+ d.getMinutes() +"-"+ d.getSeconds() +"-"+ d.getMilliseconds();
              const namefile =  st + "-"+ this.user_id+ "-"+ 'cam.jpg' ; 
              let options1: FileUploadOptions = {
                fileKey: 'file',
                fileName: namefile,
                headers: {}
              }
              const fileTransfer: FileTransferObject = this.transfer.create();
              loading.present();
              fileTransfer.upload(imageData, 'http://159.203.82.152/custom-upload.php', options1).then((data) => {
                this.rtm_img = namefile;
                }, (err) => {
                  alert('Error. Intente de nuevo')
                });
              }, (err) => {
                // Handle error
            });
          }
        },{
          text: 'Galeria',
          handler: () => {
            console.log('Gal clicked');
            const options: CameraOptions = {
              quality: 100,
              sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
              destinationType: this.camera.DestinationType.FILE_URI,
              encodingType: this.camera.EncodingType.JPEG,
              mediaType: this.camera.MediaType.PICTURE,
              correctOrientation : false,
              // targetWidth: 200,
              // targetHeight: 200,
            }
            this.camera.getPicture(options).then((imageData) => {
              let d = new Date();
              let st = d.getDate() +"-"+ d.getMonth() +"-"+ d.getFullYear() +"-"+ d.getHours() +"-"+ d.getMinutes() +"-"+ d.getSeconds() +"-"+ d.getMilliseconds();
              const namefile =  st + "-"+ this.user_id+ "-"+ 'gal.jpg' ; 
              let options1: FileUploadOptions = {
                fileKey: 'file',
                fileName: namefile,
                headers: {}
              }
              const fileTransfer: FileTransferObject = this.transfer.create();
              loading.present();
              fileTransfer.upload(imageData, 'http://159.203.82.152/custom-upload.php', options1).then((data) => {
                this.rtm_img = namefile;
                }, (err) => {
                  alert('Error. Intente de nuevo')
                });
          });
        }
        }
      ]
    });
    actionSheet.present();
  }

  upload_property(){
    let loading = this.loadingCtrl.create({
      content: "Espere",
      duration: 5000
    });
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Tarjeta de propieda',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Destructive clicked');
          }
        },{
          text: 'Camara',
          handler: () => {
            console.log('Cam clicked');
            const options: CameraOptions = {
              quality: 70,
              sourceType: this.camera.PictureSourceType.CAMERA,
              destinationType: this.camera.DestinationType.NATIVE_URI,
              encodingType: this.camera.EncodingType.JPEG,
              mediaType: this.camera.MediaType.PICTURE ,
              correctOrientation : false
              // targetWidth: 200,
              // targetHeight: 200,
            }
            this.camera.getPicture(options).then((imageData) => {
              let d = new Date();
              let st = d.getDate() +"-"+ d.getMonth() +"-"+ d.getFullYear() +"-"+ d.getHours() +"-"+ d.getMinutes() +"-"+ d.getSeconds() +"-"+ d.getMilliseconds();
              const namefile =  st + "-"+ this.user_id+ "-"+ 'cam.jpg' ; 
              let options1: FileUploadOptions = {
                fileKey: 'file',
                fileName: namefile,
                headers: {}
              }
              const fileTransfer: FileTransferObject = this.transfer.create();
              loading.present();
              fileTransfer.upload(imageData, 'http://159.203.82.152/custom-upload.php', options1).then((data) => {
                this.property_img = namefile;
                }, (err) => {
                  alert('Error. Intente de nuevo')
                });
              }, (err) => {
                // Handle error
            });
          }
        },{
          text: 'Galeria',
          handler: () => {
            console.log('Gal clicked');
            const options: CameraOptions = {
              quality: 100,
              sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
              destinationType: this.camera.DestinationType.FILE_URI,
              encodingType: this.camera.EncodingType.JPEG,
              mediaType: this.camera.MediaType.PICTURE,
              correctOrientation : false,
              // targetWidth: 200,
              // targetHeight: 200,
            }
            this.camera.getPicture(options).then((imageData) => {
              let d = new Date();
              let st = d.getDate() +"-"+ d.getMonth() +"-"+ d.getFullYear() +"-"+ d.getHours() +"-"+ d.getMinutes() +"-"+ d.getSeconds() +"-"+ d.getMilliseconds();
              const namefile =  st + "-"+ this.user_id+ "-"+ 'gal.jpg' ; 
              let options1: FileUploadOptions = {
                fileKey: 'file',
                fileName: namefile,
                headers: {}
              }
              const fileTransfer: FileTransferObject = this.transfer.create();
              loading.present();
              fileTransfer.upload(imageData, 'http://159.203.82.152/custom-upload.php', options1).then((data) => {
                this.property_img = namefile;
                }, (err) => {
                  alert('Error. Intente de nuevo')
                });
          });
        }
        }
      ]
    });
    actionSheet.present();
  }

  upload_propertyB(){
    let loading = this.loadingCtrl.create({
      content: "Espere",
      duration: 5000
    });
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Tarjeta de propiedad',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Destructive clicked');
          }
        },{
          text: 'Camara',
          handler: () => {
            console.log('Cam clicked');
            const options: CameraOptions = {
              quality: 70,
              sourceType: this.camera.PictureSourceType.CAMERA,
              destinationType: this.camera.DestinationType.NATIVE_URI,
              encodingType: this.camera.EncodingType.JPEG,
              mediaType: this.camera.MediaType.PICTURE ,
              correctOrientation : false
              // targetWidth: 200,
              // targetHeight: 200,
            }
            this.camera.getPicture(options).then((imageData) => {
              let d = new Date();
              let st = d.getDate() +"-"+ d.getMonth() +"-"+ d.getFullYear() +"-"+ d.getHours() +"-"+ d.getMinutes() +"-"+ d.getSeconds() +"-"+ d.getMilliseconds();
              const namefile =  st + "-"+ this.user_id+ "-"+ 'cam.jpg' ; 
              let options1: FileUploadOptions = {
                fileKey: 'file',
                fileName: namefile,
                headers: {}
              }
              const fileTransfer: FileTransferObject = this.transfer.create();
              loading.present();
              fileTransfer.upload(imageData, 'http://159.203.82.152/custom-upload.php', options1).then((data) => {
                this.property_imgB = namefile;
                }, (err) => {
                  alert('Error. Intente de nuevo')
                });
              }, (err) => {
                // Handle error
            });
          }
        },{
          text: 'Galeria',
          handler: () => {
            console.log('Gal clicked');
            const options: CameraOptions = {
              quality: 100,
              sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
              destinationType: this.camera.DestinationType.FILE_URI,
              encodingType: this.camera.EncodingType.JPEG,
              mediaType: this.camera.MediaType.PICTURE,
              correctOrientation : false,
              // targetWidth: 200,
              // targetHeight: 200,
            }
            this.camera.getPicture(options).then((imageData) => {
              let d = new Date();
              let st = d.getDate() +"-"+ d.getMonth() +"-"+ d.getFullYear() +"-"+ d.getHours() +"-"+ d.getMinutes() +"-"+ d.getSeconds() +"-"+ d.getMilliseconds();
              const namefile =  st + "-"+ this.user_id+ "-"+ 'gal.jpg' ; 
              let options1: FileUploadOptions = {
                fileKey: 'file',
                fileName: namefile,
                headers: {}
              }
              const fileTransfer: FileTransferObject = this.transfer.create();
              loading.present();
              fileTransfer.upload(imageData, 'http://159.203.82.152/custom-upload.php', options1).then((data) => {
                this.property_imgB = namefile;
                }, (err) => {
                  alert('Error. Intente de nuevo')
                });
          });
        }
        }
      ]
    });
    actionSheet.present();
  }

  upload_policy(){
    let loading = this.loadingCtrl.create({
      content: "Espere",
      duration: 5000
    });
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Poliza contra todo riesgo',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Destructive clicked');
          }
        },{
          text: 'Camara',
          handler: () => {
            console.log('Cam clicked');
            const options: CameraOptions = {
              quality: 70,
              sourceType: this.camera.PictureSourceType.CAMERA,
              destinationType: this.camera.DestinationType.NATIVE_URI,
              encodingType: this.camera.EncodingType.JPEG,
              mediaType: this.camera.MediaType.PICTURE ,
              correctOrientation : false
              // targetWidth: 200,
              // targetHeight: 200,
            }
            this.camera.getPicture(options).then((imageData) => {
              let d = new Date();
              let st = d.getDate() +"-"+ d.getMonth() +"-"+ d.getFullYear() +"-"+ d.getHours() +"-"+ d.getMinutes() +"-"+ d.getSeconds() +"-"+ d.getMilliseconds();
              const namefile =  st + "-"+ this.user_id + "-"+ 'cam.jpg' ; 
              let options1: FileUploadOptions = {
                fileKey: 'file',
                fileName: namefile,
                headers: {}
              }
              const fileTransfer: FileTransferObject = this.transfer.create();
              loading.present();
              fileTransfer.upload(imageData, 'http://159.203.82.152/custom-upload.php', options1).then((data) => {
                this.policy_img = namefile;
                }, (err) => {
                  alert('Error. Intente de nuevo')
                });
              }, (err) => {
                // Handle error
            });
          }
        },{
          text: 'Galeria',
          handler: () => {
            console.log('Gal clicked');
            const options: CameraOptions = {
              quality: 100,
              sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
              destinationType: this.camera.DestinationType.FILE_URI,
              encodingType: this.camera.EncodingType.JPEG,
              mediaType: this.camera.MediaType.PICTURE,
              correctOrientation : false,
              // targetWidth: 200,
              // targetHeight: 200,
            }
            this.camera.getPicture(options).then((imageData) => {
              let d = new Date();
              let st = d.getDate() +"-"+ d.getMonth() +"-"+ d.getFullYear() +"-"+ d.getHours() +"-"+ d.getMinutes() +"-"+ d.getSeconds() +"-"+ d.getMilliseconds();
              const namefile =  st + "-"+ this.user_id + "-"+ 'gal.jpg' ; 
              let options1: FileUploadOptions = {
                fileKey: 'file',
                fileName: namefile,
                headers: {}
              }
              const fileTransfer: FileTransferObject = this.transfer.create();
              loading.present();
              fileTransfer.upload(imageData, 'http://159.203.82.152/custom-upload.php', options1).then((data) => {
                this.policy_img = namefile;
                }, (err) => {
                  alert('Error. Intente de nuevo')
                });
          });
        }
        }
      ]
    });
    actionSheet.present();
  }

}
