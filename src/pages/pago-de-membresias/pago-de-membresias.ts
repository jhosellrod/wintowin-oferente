import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Storage } from '@ionic/storage'
import { CorekProvider } from '../../providers/corek/corek';

@Component({
  selector: 'page-pago-de-membresias',
  templateUrl: 'pago-de-membresias.html'
})
export class PagoDeMembresiasPage {
  public items = [];
  public id : any;
  public amount: any;
  public datePay: any;
  public flag = false;
  constructor(private corek: CorekProvider, private storage: Storage, public navCtrl: NavController, private iab: InAppBrowser) {
    this.storage.get('ID').then((id)=>{
      this.id = id;
    });
  }

  ionViewDidEnter(){
    let time = Date.now().toString()+Math.random();
    this.corek.socket.emit('query',{'event':time + 'queryT', 'querystring':"SELECT * FROM `wp_posts` WHERE `post_author` = "+this.id+" AND `post_status` LIKE 'successful' AND `post_type` LIKE 'transaction'"});
    this.corek.socket.on(time + 'queryT', (result, key)=>{
      if (result.length > 0){
        this.flag = true;
        this.amount = result[0].post_content;
        this.datePay = result[0].post_date;
        this.items.push({'amount': this.amount,'datePay':this.datePay});
      }else{
        this.flag = false;
      }
    });
  }
}