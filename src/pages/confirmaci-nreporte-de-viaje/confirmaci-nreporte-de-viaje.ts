import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { CrearUnViajePage } from '../crear-un-viaje/crear-un-viaje'

@Component({
  selector: 'page-confirmaci-nreporte-de-viaje',
  templateUrl: 'confirmaci-nreporte-de-viaje.html'
})
export class ConfirmaciNReporteDeViajePage {
public infoTrip: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams
    ) {         
      this.infoTrip = this.navParams.get('data');
      console.log(this.infoTrip);
      
    }

  goToEstadoDelViaje(){
    this.navCtrl.setRoot(CrearUnViajePage);
    this.navCtrl.popToRoot();
  }
  
}
