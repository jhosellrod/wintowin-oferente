import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { RequisitosPage } from '../requisitos/requisitos';
import { RegistroConductor1Page } from '../registro-conductor1/registro-conductor1';


@Component({
  selector: 'page-bienvenida',
  templateUrl: 'bienvenida.html'
})
export class BienvenidaPage {

  constructor(public navCtrl: NavController) {
  }
  goToRequisitos(params){
    if (!params) params = {};
    this.navCtrl.push(RequisitosPage);
  }goToRegistroConductor1(params){
    if (!params) params = {};
    this.navCtrl.push(RegistroConductor1Page);
  }
}
