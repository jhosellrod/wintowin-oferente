import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-resultados-viajes-realizados',
  templateUrl: 'resultados-viajes-realizados.html'
})
export class ResultadosViajesRealizadosPage {

  trips;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.trips= this.navParams.get('result');
    console.log(this.trips);
  }
  
}
