import { Component, NgZone} from '@angular/core';
import { NavController, LoadingController, ToastController, AlertController} from 'ionic-angular';
import { ConfirmaciNCreaciNDeViajePage } from '../confirmaci-ncreaci-nde-viaje/confirmaci-ncreaci-nde-viaje';
import { MiActualViajePage } from '../mi-actual-viaje/mi-actual-viaje'
import { Geolocation } from '@ionic-native/geolocation';
import { CorekProvider } from '../../providers/corek/corek';
import { Storage } from '@ionic/storage';
import { IniciaSesionPage } from '../inicia-sesion/inicia-sesion';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { SplashScreen } from '@ionic-native/splash-screen';
import { MiPerfilPage } from '../mi-perfil/mi-perfil';
import { FCM } from '@ionic-native/fcm'
import moment from 'moment';
import { PayPage } from '../pay/pay';
declare var google;

@Component({
  selector: 'page-crear-un-viaje',
  templateUrl: 'crear-un-viaje.html'
})
export class CrearUnViajePage {

  // Maps begin
  service;
  position;
  // begin
  autocompleteBeginItems;
  autocompleteBegin;
  showList_begin;
  flag_begin = 2;
  // end
  autocompleteEndItems;
  autocompleteEnd;
  showList_end;
  flag_end = 2;
  // Maps end
  public id: any;
  min_time;
  user_name;
  user_email;
  user_car;
  public polyline;

  constructor(public splashScreen: SplashScreen, private fcm: FCM, private locationAccuracy: LocationAccuracy, public navCtrl: NavController, public alertCtrl: AlertController, public loadingCtrl: LoadingController, public toast: ToastController, private geolocation: Geolocation, private corek: CorekProvider, private storage: Storage, private zone: NgZone) {
    
    this.storage.get('user_code').then((key) => {
      this.code = key;
    });

    this.storage.get('user_car').then((key) => {
      this.user_car = key;
    });

    this.storage.get('user_mail').then((key) => {
      this.user_email = key;
    });

    this.storage.get('user_name').then((key) => {
      this.user_name = key;
    });

    this.storage.get('user_id').then((data)=>{
      this.id = data;
      this.fcm.getToken().then(token=>{
        let cnow = Date.now().toString()+'newnotification'+Math.random();
        this.corek.socket.emit('newnotification',{'id_user':this.id,'token':token,'event':cnow});
      });
      this.fcm.subscribeToTopic('notification'+this.id);
    });

    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if(canRequest) {
        this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_BALANCED_POWER_ACCURACY).then(
          () =>
          console.log('acept')
          ,
          error => console.log('Error requesting location permissions', error)
        );
      }
    });

    this.min_time = moment().format('HH:mm');
    
    this.autocompleteBeginItems = [];
    this.autocompleteBegin = {query: ''};

    this.autocompleteEndItems = [];
    this.autocompleteEnd = {query: ''};
  }

  seats; 
  date;
  code;
  position_begin;
  position_end; 
  toggleClicked = false; 
  trip_id;
  trip;
  community;
  active_car = false;

  
  ionViewDidEnter(){
    // Listening newpost in webapp

    //Get trips from user
    let n = Date.now().toString+'cone'+Math.random();
    this.corek.ConnectCorekconfig(n);
    this.corek.socket.on(n, (dat, key)=>{
      console.log(dat)
      if (dat.conf == true){
        this.storage.get('user_id').then((id) => {
            this.id = id;
            this.corek.socket.on('close' + this.id ,  (data , key ) => {
              if(data.condition.status == 1 || data.condition.status == 3){
                console.log("activ");
              }else{
                this.clearData();
                this.navCtrl.setRoot(IniciaSesionPage);
                this.navCtrl.popToRoot();
              }
            });
          let nf1 = Date.now().toString() +'get_user'+Math.random();
          this.corek.socket.emit('get_users', {'condition':{'ID':id}, 'event':nf1});
          this.corek.socket.on(nf1,(users)=>{ 
            if (moment().isSameOrAfter(moment(users[0].user_registered_end))){
              let nf2 = Date.now().toString()+'update'+Math.random();
              this.corek.socket.emit('update_user',{'set':{'user_status':2},'condition':{'user_activation_key':users[0].user_activation_key,},'event':nf2,});
              this.corek.socket.on(nf2,(data)=>{
                let alert = this.alertCtrl.create({
                  title: 'Fecha de prueba caducada',
                  subTitle: 'Se ha acabado la fecha de prueba, para ingresar adquiera una licencia',
                  buttons: ['OK'],
                });
                alert.present();
                this.navCtrl.setRoot(PayPage, {licenses_end: true});
                this.navCtrl.popToRoot();
              });
            }
          });
        });
      }
    });

  }

  reload(){
    this.splashScreen.show();
    window.location.reload();
  }

  clearData(){
    this.storage.clear();
    this.fcm.unsubscribeFromTopic('notification'+this.id)
  }

  createTrip(){
    let now = new Date();
    let date = now.getFullYear()+'-'+(now.getMonth()+1)+'-'+now.getDate()+' '+this.date+':'+now.getSeconds();
    let loading = this.loadingCtrl.create({
      content: "Espere",
      duration: 15000
    });
    loading.present();
    if (this.position_begin == null){
      const confirm = this.alertCtrl.create({
        title: 'No podemos acceder a tu ubicacion',
        message: '¿Desea solucionarlo en este momento?',
        buttons: [
          {
            text: 'Si',
            handler: () => {
              this.reload();
            }
          },
          {
            text: 'No',
            handler: () => {
              console.log('Disagree clicked');
            }
          }
        ]
      });
      loading.dismiss();
      confirm.present();
    }else{
      // Get car status
      let nj1 = Date.now().toString+'cars'+Math.random();
      this.corek.socket.emit('query_post', { 'condition': {"post_type":"cars", "comment_status":this.user_car} ,'key': 'notificacion', 'event': nj1});
      this.corek.socket.on(nj1, (car, key) => {
        console.log(car);
        if (car.length > 0) {
          if (car[0].post_parent == 1){
            let car_data = {'placa': car[0].post_content_filtered, 'Modelo': car[0].post_excerpt+" "+car[0].pinged}
             // Get Trips
            let nf = Date.now().toString+'prueba'+car[0].ID+Math.random();
            this.corek.socket.emit('query_post', { 'condition': {"post_type":"trips", "post_parent":0, "post_excerpt":this.code} ,'key': 'notificacion', 'event': nf});
            this.corek.socket.on(nf, (data, key) => {
              let n = data.length
              if(n == 0){
                let distance = this.distance(this.position_begin.latitud, this.position_begin.longitud, this.position_end.latitud, this.position_end.longitud);
                // Get Communitys
                let nj = Date.now().toString+'community'+Math.random();
                this.corek.socket.emit("get_user_meta",{"condition":{"user_id":this.id, 'meta_key': "community"}, "event":nj});
                this.corek.socket.on(nj,(community,key)=> {
                  if (community.length > 0 && JSON.parse(community[0].meta_value)[0] != ''){
                    this.community = JSON.parse(community[0].meta_value);
                    if(this.community.length == 1){
                      let nf1 = Date.now().toString()+"ins"+Math.random();
                      this.corek.socket.emit('insert_post',{'condition':{'post_type':'trips', 'post_author': this.id, 'post_date':date,'post_status': this.community[0],'post_title':JSON.stringify(car_data),'post_excerpt': this.code,'menu_order': parseInt(this.seats),'post_content': JSON.stringify(this.position_begin),'post_password': JSON.stringify(this.position_end), 'to_ping':distance.toFixed(2), 'polilyne':this.polyline.__zone_symbol__value},'event':nf1});
                      this.corek.socket.on(nf1, (result, key)=>{
                        this.storage.set('trip_id', result.insertId);
                        this.storage.set('seats', parseInt(this.seats));
                        loading.dismiss();
                        this.navCtrl.setRoot(ConfirmaciNCreaciNDeViajePage);
                        this.navCtrl.popToRoot();
                      });
                    }else if(this.community.length == 2){
                      let nf1 = Date.now().toString()+"ins"+Math.random();
                      this.corek.socket.emit('insert_post',{'condition':{'post_type':'trips', 'post_author': this.id, 'post_date':date, 'post_status': this.community[0], 'comment_status': this.community[1], 'post_title':JSON.stringify(car_data), 'post_excerpt': this.code, 'menu_order': parseInt(this.seats),'post_content': JSON.stringify(this.position_begin),'post_password': JSON.stringify(this.position_end), 'to_ping':distance.toFixed(2), 'polilyne':this.polyline.__zone_symbol__value},'event':nf1});
                      this.corek.socket.on(nf1, (result, key)=>{
                        this.storage.set('trip_id', result.insertId);
                        this.storage.set('seats', parseInt(this.seats));
                        loading.dismiss();
                        this.navCtrl.setRoot(ConfirmaciNCreaciNDeViajePage);
                        this.navCtrl.popToRoot();
                      });
                    }else if (this.community.length == 3){
                      let nf1 = Date.now().toString()+"ins"+Math.random();
                      this.corek.socket.emit('insert_post',{'condition':{'post_type':'trips', 'post_author': this.id, 'post_date':date, 'post_status': this.community[0], 'comment_status': this.community[1], 'ping_status': this.community[2], 'post_title':JSON.stringify(car_data), 'post_excerpt': this.code, 'menu_order': parseInt(this.seats),'post_content': JSON.stringify(this.position_begin),'post_password': JSON.stringify(this.position_end), 'to_ping':distance.toFixed(2), 'polilyne':this.polyline.__zone_symbol__value},'event':nf1});
                      this.corek.socket.on(nf1, (result, key)=>{
                        this.storage.set('trip_id', result.insertId);
                        this.storage.set('seats', parseInt(this.seats));
                        loading.dismiss();
                        this.navCtrl.setRoot(ConfirmaciNCreaciNDeViajePage);
                        this.navCtrl.popToRoot();
                      });
                    }
                    this.sendMail();
                  }else{
                    const confirm = this.alertCtrl.create({
                      title: 'Aún no seleccionas tus comunidades',
                      message: 'Puede seleccionarlas desde su perfil ¿desea hacerlo en este momento?',
                      buttons: [
                        {
                          text: 'Si',
                          handler: () => {
                            this.navCtrl.setRoot(MiPerfilPage);
                            this.navCtrl.popToRoot();
                          }
                        },
                        {
                          text: 'No',
                          handler: () => {
                            let nf1 = Date.now().toString()+"ins"+Math.random();
                            this.corek.socket.emit('insert_post',{'condition':{'post_type':'trips', 'post_author': this.id, 'post_excerpt': this.code,'post_date': date,'menu_order': parseInt(this.seats), 'post_title':JSON.stringify(car_data),'post_content': JSON.stringify(this.position_begin),'post_password': JSON.stringify(this.position_end), 'to_ping':distance.toFixed(2), 'polilyne':this.polyline.__zone_symbol__value},'event':nf1});
                            this.corek.socket.on(nf1, (result, key)=>{
                              console.log(result)
                              this.storage.set('trip_id', result.insertId);
                              this.storage.set('seats', parseInt(this.seats));
                              this.sendMail();
                              this.navCtrl.setRoot(ConfirmaciNCreaciNDeViajePage);
                              this.navCtrl.popToRoot();
                            });
                          }
                        }
                      ]
                    });
                    loading.dismiss();
                    confirm.present();
                  }
                });
              }else{
                let toast = this.toast.create({
                  message: 'Su vehículo ya esta siendo usado en otro viaje',
                  duration: 6000
                });
                loading.dismiss();
                toast.present();
              }
            });
          }else{
            let alert = this.alertCtrl.create({
              title: 'Su vehículo aún no es activado',
              subTitle: 'Espere a que los administradores lo hagan',
              buttons: ['OK']
            });
            loading.dismiss();
            alert.present();
          }
        }else{
          let alert = this.alertCtrl.create({
            title: 'Su vehículo ha sido eliminado',
            subTitle: 'Por favor póngase en contacto con el equipo de soporte',
            buttons: ['OK']
          });
          loading.dismiss();
          alert.present();
        }
      });
    } 
  }

  sendMail(){
    console.log('sendMail');
    let nf3 = Date.now().toString()+'send_mail'+Math.random();
    let mail = '<div style="position:relative;min-width:320px;">'+
    '<div class="clearfix borderbox" style="z-index:1;min-height:535px;background-image:none;border-width:0px;border-color:#000000;background-color:transparent;padding-bottom:62px;width:100%;max-width:800px;margin-left:auto;margin-right:auto;">'+
    '<div class="clearfix colelem" style="z-index:2;background-color:#000000;position:relative;margin-right:-10000px;width:100%;">'+
    '<div class="grpelem"><div></div></div>'+
    '<div class="clearfix grpelem" style="z-index:4;min-height:17px;background-color:transparent;line-height:18px;color:#FFFFFF;font-size:15px;font-family:roboto, sans-serif;font-weight:300;position:relative;margin-right:-10000px;width:19.25%;left:7.13%;" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true">'+
    '<p>¡Hola '+this.user_name+'!</p></div></div>'+
    '<div class="colelem" style="z-index:3;height:133px;opacity:1;filter:alpha(opacity=100);margin-top:91px;position:relative;width:18.13%;margin-left:40.13%; background-size: contain;">'+
    '<img src="http://159.203.82.152/assets/logo.png" alt=""></div>'+
    '<div class="clearfix colelem" style="z-index:13;min-height:17px;background-color:transparent;line-height:22px;color:#000000;font-size:18px;font-family:roboto, sans-serif;font-weight:700;margin-top:78px;position:relative;width:46.13%;margin-left:26.13%;">'+
    '<p>¡MUY BIEN! HAS CREADO SATISFACTORIAMENTE TU VIAJE.</p></div>'+
    '<div class="clearfix colelem" style="z-index:17;min-height:17px;background-color:transparent;line-height:22px;color:#000000;font-size:18px;font-family:roboto, sans-serif;font-weight:700;margin-top:77px;position:relative;width:46.13%;margin-left:26.13%;" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true">'+
    '<p>Debes estar atento en la App móvil a la pantalla “Estado del viaje”, ya que allí te aparecerán los usuarios que desean viajar contigo... recuerda que debes aceptarlos.</p></div>'+
    '<div class="clearfix colelem" style="background-color:#000000; height:48px; z-index:8;margin-top:111px;width:100%;">'+
    '<div class="grpelem" style="display:inline;float:left; z-index:8;background-color:#000000;position:relative;margin-right:-10000px;width:100%;">'+
    '<div class="fluid_height_spacer"></div></div><div class="clearfix grpelem" style="height: 17px; display:inline;float:left;z-index:9;min-height:17px;background-color:transparent;line-height:18px;color:#FFFFFF;font-size:15px;font-family:roboto, sans-serif;font-weight:300;position:relative;margin-right:-10000px;width:10%;left:43%; ">'+
    '<p>WIN to WIN</p></div></div>'+
    '<div class="clearfix colelem" style="z-index:21;min-height:17px;background-color:transparent;line-height:14px;color:#000000;font-size:12px;font-family:roboto, sans-serif;font-weight:300;margin-top:9px;position:relative;width:45.88%;margin-left:28.38%;" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true">'+
    '<p>¿Tienes dudas? Envíanos un mensaje a soporte@wintowin.co</p></div><div class="verticalspacer" data-offset-top="600" data-content-above-spacer="599" data-content-below-spacer="61" data-sizePolicy="fixed" data-pintopage="page_fixedLeft"></div>'
    +'</div></div>';
    this.corek.socket.emit('confsmtp',{'key':'confsmtp','token':8,'event':nf3});
    this.corek.socket.on(nf3, (data, key)=>{
      console.log(data);
      console.log(data.key);
      if(key == 'confsmtp'){
        console.log('validar aqui');
        this.corek.socket.emit('sendemail',{'event':event,'key':'sendemail',
        'from':'"WinToWin" <noreply@wintowin.com.co>',
        'to':this.user_email,
        'subject':'Creación de viaje',
        'html':mail
        });    
      }
    });
  }

  distance(lat1, lon1, lat2, lon2){
    let rad = function(x) {return x*Math.PI/180;}
    let R = 6371; // radio de la tierra en km
    let dLat  = rad( lat2 - lat1 );
    let dLong = rad( lon2 - lon1 );
    let a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(rad(lat1)) * Math.cos(rad(lat2)) * Math.sin(dLong/2) * Math.sin(dLong/2);
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    let d = R * c; 
    return d;
  }

  getLocation(){
    console.log('get location');
    this.toggleClicked = !this.toggleClicked;
    this.autocompleteBegin.query = '';
    this.geolocation.getCurrentPosition().then((result)=>{
      this.position = {lat:result.coords.latitude, lng: result.coords.longitude}
      let geocoder = new google.maps.Geocoder;
      this.geocodeAddress(geocoder,(val)=> {
        this.position_begin = {'latitud':result.coords.latitude, 'longitud': result.coords.longitude, 'name':val};
      });
    }).catch((error)=>{
      console.log('el error'+error);
    });
  }

  geocodeAddress(geocoder, callback) {
    let name_adress;
    geocoder.geocode({'location': this.position}, function(results, status) {
      if (status === 'OK') {
        name_adress = results[0].formatted_address;
        callback(name_adress);
      } else {
        console.log(status)
      }
    });
  }

  getAddress_begin(){
    if(this.service == null){
      this.service = new google.maps.places.AutocompleteService();
    }
    if (this.showList_begin == 1.5){
      this.flag_begin = 2;
    }
    this.showList_begin = this.flag_begin / 2;
    if (this.autocompleteBegin.query == '') {
      this.autocompleteBeginItems = [];
      return;
     }
 
     let me = this;
     this.service.getPlacePredictions({
     input: this.autocompleteBegin.query,
     componentRestrictions: {
       country: 'co'
     }
    }, (predictions, status) => {
      me.autocompleteBeginItems = [];
 
    me.zone.run(() => {
      if (predictions != null) {
         predictions.forEach((prediction) => {
           me.autocompleteBeginItems.push(prediction.description);
         });
        }
      });
    });
  }

  selectPlace_begin(item){
    this.autocompleteBegin.query = item;
    this.flag_begin = 3;
    this.geoCode_begin(item);
  }

  geoCode_begin(address:any) {
    let geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': address, }, (results, status) => {
      this.position_begin = {'latitud':results[0].geometry.location.lat(), 'longitud': results[0].geometry.location.lng(), 'name':results[0].formatted_address};
      console.log(this.position_begin, this.position_end);
      if (this.position_begin != undefined && this.position_end != undefined){
        this.getRoute();
      }
    });
  }

  getAddress_end(){
  if(this.service == null){
    this.service = new google.maps.places.AutocompleteService();
  }
  if (this.showList_end == 1.5){
    this.flag_end = 2;
  }
  this.showList_end = this.flag_end / 2;
  if (this.autocompleteEnd.query == '') {
    this.autocompleteEndItems = [];
    return;
   }

   let me = this;
   this.service.getPlacePredictions({
   input: this.autocompleteEnd.query,
   componentRestrictions: {
     country: 'co'
   }
  }, (predictions, status) => {
    me.autocompleteEndItems = [];

  me.zone.run(() => {
    if (predictions != null) {
       predictions.forEach((prediction) => {
         me.autocompleteEndItems.push(prediction.description);
       });
      }
    });
  });
  }

  selectPlace_end(item){
    this.autocompleteEnd.query = item;
    this.flag_end = 3;
    this.geoCode_end(item);
  }

  geoCode_end(address:any) {
    let geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': address, }, (results, status) => {
      this.position_end = {'latitud':results[0].geometry.location.lat(), 'longitud': results[0].geometry.location.lng(), 'name':results[0].formatted_address};
      console.log(this.position_begin, this.position_end);
      if (this.position_begin != undefined && this.position_end != undefined){
        this.getRoute();
      }
    });
  }

  getRoute(){
    let directionsService = new google.maps.DirectionsService;
    let start = {'lat': this.position_begin.latitud, 'lng': this.position_begin.longitud}
    let end = {'lat': this.position_end.latitud, 'lng': this.position_end.longitud}
    this.polyline = new Promise(function(resolve, reject) {
      directionsService.route({origin: start, destination: end, travelMode: 'DRIVING'}, function(response, status) {
        if (status === 'OK') {
          resolve(response.routes[0].overview_polyline);
        }else {
          reject('Directions request failed due to ' + status);
        }
      });
    });
  }
  

  goToVerViaje(){
    this.navCtrl.setRoot(MiActualViajePage);
    this.navCtrl.popToRoot();
  }



}
