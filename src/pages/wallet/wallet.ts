import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { CorekProvider } from '../../providers/corek/corek';


@Component({
  selector: 'page-wallet',
  templateUrl: 'wallet.html'
})
export class WalletPage {

  wallet;
  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, private storage: Storage, private corek: CorekProvider) {}

  ionViewDidEnter(){
    let loader = this.loadingCtrl.create({
      content: "Espere",
      duration: 15000
    });
    loader.present();
    this.storage.get('user_id').then((id)=>{
      let nf = Date.now().toString()+'waller'+Math.random();
      this.corek.socket.emit('get_users',{"condition":{'ID':id},'event':nf +'get_user'});
      this.corek.socket.on(nf+'get_user', (data,key)=>{
        this.wallet = data[0].wallet;
        loader.dismiss();
      });
    });
  }
  
}
