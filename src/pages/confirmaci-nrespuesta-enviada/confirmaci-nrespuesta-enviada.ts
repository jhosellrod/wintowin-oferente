import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { EstadoDelViajePage } from '../estado-del-viaje/estado-del-viaje';
import { VerUsuarioPage } from '../ver-usuario/ver-usuario';
import { ConfirmaciNReporteDeViajePage } from '../confirmaci-nreporte-de-viaje/confirmaci-nreporte-de-viaje';

@Component({
  selector: 'page-confirmaci-nrespuesta-enviada',
  templateUrl: 'confirmaci-nrespuesta-enviada.html'
})
export class ConfirmaciNRespuestaEnviadaPage {

  constructor(public navCtrl: NavController) {
  }
  goToEstadoDelViaje(params){
    if (!params) params = {};
    this.navCtrl.push(EstadoDelViajePage);
  }goToVerUsuario(params){
    if (!params) params = {};
    this.navCtrl.push(VerUsuarioPage);
  }goToConfirmaciNReporteDeViaje(params){
    if (!params) params = {};
    this.navCtrl.push(ConfirmaciNReporteDeViajePage);
  }
}
