import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { CrearUnViajePage } from '../crear-un-viaje/crear-un-viaje';
import { ConfirmaciNCreaciNDeViajePage } from '../confirmaci-ncreaci-nde-viaje/confirmaci-ncreaci-nde-viaje';
import { EstadoDelViajePage } from '../estado-del-viaje/estado-del-viaje';
import { VerUsuarioPage } from '../ver-usuario/ver-usuario';
import { ConfirmaciNReporteDeViajePage } from '../confirmaci-nreporte-de-viaje/confirmaci-nreporte-de-viaje';
import { Contacto2Page } from '../contacto2/contacto2';
import { ConfirmaciNMensajePage } from '../confirmaci-nmensaje/confirmaci-nmensaje';
import { WalletPage } from '../wallet/wallet';

@Component({
  selector: 'page-tabs-controller',
  templateUrl: 'tabs-controller.html'
})
export class TabsControllerPage {

  tab1Root: any = CrearUnViajePage;
  tab2Root: any = WalletPage;
  tab3Root: any = Contacto2Page;
  constructor(public navCtrl: NavController) {
  }
  goToCrearUnViaje(params){
    if (!params) params = {};
    this.navCtrl.push(CrearUnViajePage);
  }goToConfirmaciNCreaciNDeViaje(params){
    if (!params) params = {};
    this.navCtrl.push(ConfirmaciNCreaciNDeViajePage);
  }goToEstadoDelViaje(params){
    if (!params) params = {};
    this.navCtrl.push(EstadoDelViajePage);
  }goToVerUsuario(params){
    if (!params) params = {};
    this.navCtrl.push(VerUsuarioPage);
  }goToConfirmaciNReporteDeViaje(params){
    if (!params) params = {};
    this.navCtrl.push(ConfirmaciNReporteDeViajePage);
  }goToContacto2(params){
    if (!params) params = {};
    this.navCtrl.push(Contacto2Page);
  }goToConfirmaciNMensaje(params){
    if (!params) params = {};
    this.navCtrl.push(ConfirmaciNMensajePage);
  }goToWallet(params){
    if (!params) params = {};
    this.navCtrl.push(WalletPage);
  }
}
